# Set file dir as working directory
cd "$(dirname "$0")"

# Clean up
rm -rf build/* dist/*

# Check if venv exists; create if not
echo "PREPARING PYTHON ENVIRONMENT..."
if [ ! -d "./venv" ]
then
    python3 -m venv venv
fi

# Activate venv
source venv/bin/activate

# Install/update application & dependencies
pip3 install -r requirements.txt .

# Run PyInstaller
echo "RUNNING PYINSTALLER..."
pyinstaller --noconfirm AccGTK.spec
