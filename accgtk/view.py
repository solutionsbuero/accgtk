from pathlib import Path
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk
from types import SimpleNamespace
from iso4217 import Currency


class AccView:

	@staticmethod
	def get_glade() -> Path:
		glade: Path = Path(__file__).resolve().parent / "mainwindow.glade"
		if not glade.is_file():
			raise FileNotFoundError("Glade file not found! I was looking at {}".format(str(glade)))
		return glade

	def __init__(self):
		glade = self.get_glade()
		builder = Gtk.Builder()
		self.elements = builder.new_from_file(str(glade))
		self.window: Gtk.ApplicationWindow = self.elements.get_object("mainwindow")
		self.init_currency_dropdown()

	def get_elements(self):
		return self.elements

	def get_element(self, element_id: str):
		return self.elements.get_object(element_id)

	def show_main(self):
		self.window.connect("destroy", Gtk.main_quit)
		self.window.show_all()
		self.elements.get_object("status_label_container").hide()
		self.elements.get_object("functionality_button_git").hide()
		Gdk.threads_init()
		Gtk.main()

	def get_dropdowns(self):
		return SimpleNamespace(
			organizations=self.get_element("organization_dropdown"),
			customers=self.get_element("customer_selection_dropdown"),
			projects=self.get_element("project_selection_dropdown"),
			quotes=self.get_element("quote_selection_dropdown"),
			invoices=self.get_element("invoice_selection_dropdown"),
			invoice_extended_by=self.get_element("invoice_extended_by_dropdown"),
			reminders=self.get_element("reminder_selection_dropdown"),
			reminder_exented_by=self.get_element("reminder_extended_by_dropdown"),
			expenses=self.get_element("expense_selection_dropdown"),
			expense_advenved_by=self.get_element("expense_advanced_by_dropdown"),
			expense_payment_mode=self.get_element("expense_payment_method_dropdown"),
			employee_selection=self.get_element("employee_selection_dropdown")
		)

	def init_currency_dropdown(self):
		currency_list = Gtk.ListStore(str, str)
		currency_list.append(['- not set -', "0"])
		for i in sorted(Currency, key=lambda x: x.value):
			currency_list.append([i.value, i.value])
		self.get_element("organization_currency_dropdown").set_model(currency_list)
