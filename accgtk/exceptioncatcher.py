from accthymemodels.meta import Singleton
from functools import wraps
from accgtk.threadcontrol import guithread
import traceback
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import GLib


def catch_exception(f):
	@wraps(f)
	def catch(*args, **kwargs):
		try:
			return f(*args, **kwargs)
		except Exception as e:
			tb = traceback.format_exc()
			catcher = ExceptionCatcher()
			catcher.show_error(e, tb)
	return catch


class ExceptionCatcher(metaclass=Singleton):

	def __init__(self):
		self.elements = None
		self.error_dialog = None
		self.exception_reported = True

	def set_elements(self, elements):
		self.elements = elements
		self.error_dialog = self.elements.get_object("error_dialog")
		self.attach_events()

	def attach_events(self):
		self.elements.get_object("error_dialog").connect("delete_event", self.hide_dialog)
		self.elements.get_object("error_dialog_close_button").connect("clicked", self.hide_dialog)

	def hide_dialog(self, *args):
		self.exception_reported = True
		GLib.idle_add(self.error_dialog.hide)
		return True

	@guithread
	def show_error(self, e: Exception, tb):
		if self.exception_reported:
			self.elements.get_object("error_type_label").set_text(str(type(e).__name__))
			self.elements.get_object("error_message_buffer").set_text(str(e) + "\n\nTraceback:\n" + tb)
		self.exception_reported = False
		self.error_dialog.show()
