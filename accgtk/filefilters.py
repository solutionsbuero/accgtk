from typing import List
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class FileFilters:

	ALL = "all"
	MARKDOWN = "markdown"
	HTML = "html"
	PDF = "pdf"
	IMG = "img"

	@staticmethod
	def all_files() -> Gtk.FileFilter:
		filter = Gtk.FileFilter()
		filter.set_name("All files")
		filter.add_pattern("*")
		return filter

	@staticmethod
	def markdown() -> Gtk.FileFilter:
		filter = Gtk.FileFilter()
		filter.set_name("Markdown files")
		filter.add_pattern("*.markdown")
		filter.add_pattern("*.mdown")
		filter.add_pattern("*.mkdn")
		filter.add_pattern("*.mkd")
		filter.add_pattern("*.md")
		filter.add_pattern("*.txt")
		return filter

	@staticmethod
	def html() -> Gtk.FileFilter:
		filter = Gtk.FileFilter()
		filter.set_name("HTML files")
		filter.add_pattern("*.html")
		filter.add_pattern("*.htm")
		filter.add_pattern("*.xml")
		return filter

	@staticmethod
	def pdf() -> Gtk.FileFilter:
		filter = Gtk.FileFilter()
		filter.set_name("PDF Files")
		filter.add_pattern("*.pdf")
		return filter

	@staticmethod
	def img() -> Gtk.FileFilter:
		filter = Gtk.FileFilter()
		filter.set_name("Image files")
		filter.add_pattern("*.jpg")
		filter.add_pattern("*.jpeg")
		filter.add_pattern("*.png")
		filter.add_pattern("*.svg")
		filter.add_pattern("*.tiff")
		filter.add_pattern("*.tif")
		return filter

	@staticmethod
	def apply_all_filter(dialog: Gtk.FileChooserDialog):
		dialog.add_filter(FileFilters.all_files())

	@staticmethod
	def apply_markdown_filter(dialog: Gtk.FileChooserDialog):
		dialog.add_filter(FileFilters.markdown())
		dialog.add_filter(FileFilters.all_files())

	@staticmethod
	def apply_html_filter(dialog: Gtk.FileChooserDialog):
		dialog.add_filter(FileFilters.html())
		dialog.add_filter(FileFilters.all_files())

	@staticmethod
	def apply_pdf_filter(dialog: Gtk.FileChooserDialog):
		dialog.add_filter(FileFilters.pdf())
		dialog.add_filter(FileFilters.all_files())

	@staticmethod
	def apply_img_filter(dialog: Gtk.FileChooserDialog):
		dialog.add_filter(FileFilters.img())
		dialog.add_filter(FileFilters.all_files())

	@staticmethod
	def apply_filter(filter: str, dialog: Gtk.FileChooserDialog):
		if filter == FileFilters.ALL:
			FileFilters.apply_all_filter(dialog)
		elif filter == FileFilters.MARKDOWN:
			FileFilters.apply_markdown_filter(dialog)
		elif filter == FileFilters.PDF:
			FileFilters.apply_pdf_filter(dialog)
		elif filter == FileFilters.HTML:
			FileFilters.apply_html_filter(dialog)
		elif filter == FileFilters.IMG:
			FileFilters.apply_img_filter(dialog)
		else:
			raise ValueError("File filter not found")
