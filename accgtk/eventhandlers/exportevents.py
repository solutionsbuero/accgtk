from pathlib import Path
import time
import uuid
import threading
from typing import Optional
from accthymemodels.models.enums import ExportFormat
from accgtk.helpers import AccGtkHelpers
from accgtk.threadcontrol import background, guithread
from accgtk.messages import FeedbackMessages
from accgtk.eventhandlers.interfaces import Observable
from accgtk.guimodels.config import GuiConfig
from accgtk.filefilters import FileFilters
from accgtk.eventhandlers.dialogevents import DialogEvents
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GLib
from time import sleep
from accgtk.exceptioncatcher import catch_exception


class ExportEvents(Observable):

	def update_model(self, model):
		self.strategy = model
		if model is not None:
			years = sorted(list(self.strategy.get_cache().years))
		else:
			years = []
		list_store = Gtk.ListStore(str, int)
		for i in years:
			list_store.append([str(i), i])
		self.elements.get_object("year_export_dropdown").set_model(list_store)

	def attach_events(self):
		self.elements.get_object("export_path_find_button").connect("clicked", self.on_path_pick)
		self.elements.get_object("export_find_clear_button").connect("clicked", self.on_path_clear)
		self.elements.get_object("report_export_button").connect("clicked", self.launch_export)

	def on_path_pick(self, *args):
		DialogEvents.directory_chooser_dialog(self.elements, "export_path_entry")

	def on_path_clear(self, *args):
		self.elements.get_object("export_path_entry").set_text("")

	@background
	@catch_exception
	def launch_export(self, *args):
		pathstr = self.elements.get_object("export_path_entry").get_text()
		if pathstr == "":
			raise ValueError("Path may not be empty")
		p = Path(pathstr)
		year = int(self.elements.get_object("year_export_dropdown").get_active_text())
		expformat = self.elements.get_object("export_format_dropdown").get_active_text()
		if expformat == "PDF":
			if year is None:
				raise ValueError("Year must be set")
			year = int(year)
			self.strategy.export_report(year, p, ExportFormat.PDF)
		elif expformat == "YAML":
			self.strategy.export_report(year, p, ExportFormat.YAML)
		else:
			raise ValueError("Unknown export format")
