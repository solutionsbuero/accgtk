import uuid
from typing import Optional
from pathlib import Path
from accthymemodels.models.enums import ProjectAlignment
from accgtk.helpers import AccGtkHelpers
from accgtk.eventhandlers.interfaces import Observable
from accgtk.guimodels.config import GuiConfig
from accgtk.filefilters import FileFilters
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib
from accgtk.threadcontrol import guithread, background
from accgtk.helpers import AccGtkHelpers
from accgtk.guimodels.customer import GuiCustomer, GuiCustomerCreation
from accgtk.exceptioncatcher import catch_exception


class CustomerEvents(Observable):

	def dummy_callback(self, *args):
		pass

	def __init__(self, observer, strategy, elements, dropdown):
		super().__init__(observer, strategy, elements)
		self.dropdown = dropdown
		self.dropdown.extend_selection_callback(self.extended_selection_callback)
		self.customer_directory = None
		self.last_open_callback_id = None

	def attach_events(self):
		self.elements.get_object("new_customer_button").connect("clicked", self.new_customer_callback)
		self.elements.get_object("save_customer_button").connect("clicked", self.save_customer)
		self.elements.get_object("remove_customer_button").connect("clicked", self.on_delete_customer)
		self.elements.get_object("refresh_button_projects").connect("clicked", self.on_forced_update)

	'''
	Direct callbacks
	'''

	@guithread
	def set_sensitivity(self, active: bool):
		self.elements.get_object("customer_folder_open").set_sensitive(active)
		self.elements.get_object("remove_customer_button").set_sensitive(active)
		self.elements.get_object("project_selection_dropdown").set_sensitive(active)
		self.elements.get_object("new_project_button").set_sensitive(active)
		self.elements.get_object("project_form_container").set_sensitive(active)
		if active:
			pass
		else:
			pass

	@guithread
	def focus_customer(self):
		self.elements.get_object("project_form_notebook").set_current_page(0)

	@guithread
	def new_customer_callback(self, *args):
		self.dropdown.set_property("selected", "0")
		self.focus_customer()

	@guithread
	def attach_open_directory_callback(self):
		if self.last_open_callback_id is not None:
			self.elements.get_object("customer_folder_open").disconnect(self.last_open_callback_id)
		if self.customer_directory is not None:
			self.last_open_callback_id = self.elements.get_object("customer_folder_open").connect("clicked", self.open_directory_callback)
		else:
			self.last_open_callback_id = None

	@guithread
	def on_delete_customer(self, *args):
		unique = AccGtkHelpers.unique_or_none(self.dropdown.selected)
		if unique is not None:
			if self.observer.last_delete_callback_id is not None:
				self.elements.get_object("confirm_delete_yes").disconnect(self.observer.last_delete_callback_id)
			rm_customer = self.strategy.get_customer_by_unique(unique)
			self.observer.last_delete_callback_id = self.elements.get_object("confirm_delete_yes").connect("clicked", self.on_delete_customer_confirmed, rm_customer)
			self.elements.get_object("delete_confirm_dialog_label").set_text("Really delete customer \"{}\"?".format(rm_customer.name))
			self.elements.get_object("confirm_delete_dialog").show()

	def on_delete_customer_confirmed(self, button, rm_customer):
		self.delete_customer(rm_customer)

	@guithread
	def fill_project_alignment_dropdowns(self, project_alignments):
		self.elements.get_object("project_alignment_dropdown").set_model(project_alignments)

	'''
	Helper methods
	'''

	def update_model(self, model):
		self.strategy = model
		if self.strategy is None:
			self.clear_customer_list()
		else:
			self.fill_customer_list(model)

	def extended_selection_callback(self):
		newid = AccGtkHelpers.unique_or_none(self.dropdown.selected)
		if newid is None or newid == "0":
			GuiCustomer.empty_form(self.elements)
			self.observer.update_projects(None)
			self.set_sensitivity(False)
			self.customer_directory = None
		else:
			c = self.strategy.get_customer_by_unique(newid)
			guicustomer = GuiCustomer.parse_obj(c)
			guicustomer.to_gui(self.elements)
			self.customer_directory = guicustomer.yamlfile.parent
			self.observer.update_projects(guicustomer.projects)
			self.set_sensitivity(True)
			self.focus_customer()
		self.attach_open_directory_callback()
		self.prepare_project_alignment_dropdown()

	@background
	@catch_exception
	def open_directory_callback(self, *args):
		if self.customer_directory is not None:
			AccGtkHelpers.fm_open_path(self.customer_directory)

	'''
	Background worker methods
	'''

	@background
	@catch_exception
	def prepare_project_alignment_dropdown(self):
		project_alignments = Gtk.ListStore(str, str)
		for i in ProjectAlignment:
			project_alignments.append([i.value, i.value])
		self.fill_project_alignment_dropdowns(project_alignments)

	@background
	@catch_exception
	def fill_customer_list(self, arg):
		customer_list = self.strategy.get_customer_list()
		self.dropdown.update_dropdown_options(customer_list.__root__)
		self.dropdown.set_property("is_active", True)

	@background
	@catch_exception
	def clear_customer_list(self):
		self.dropdown.update_dropdown_options([])
		self.dropdown.set_property("is_active", False)

	@background
	@catch_exception
	def save_customer(self, *args):
		new_customer = GuiCustomerCreation.from_gui(self.elements)
		self.strategy.save_customer(new_customer)
		self.fill_customer_list(None)
		self.dropdown.update_dropdown_selection(selected=new_customer.unique)

	@background
	@catch_exception
	def delete_customer(self, rm_customer):
		GLib.idle_add(self.elements.get_object("confirm_delete_dialog").hide)
		self.strategy.delete_customer(rm_customer)
		self.fill_customer_list(None)
		self.dropdown.update_dropdown_selection(selected="0")

	@background
	@catch_exception
	def on_forced_update(self, *args):
		if self.strategy is not None:
			customers = self.strategy.get_customer_list(force_update=True)
			self.dropdown.update_dropdown_options(customers.__root__)
			self.dropdown.update_dropdown_selection("0")
