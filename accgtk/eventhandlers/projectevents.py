import uuid
from typing import Optional
from accthymemodels.models.enums import PaymentMode, ProjectAlignment
from accgtk.helpers import AccGtkHelpers
from accgtk.eventhandlers.interfaces import Observable
from accgtk.guimodels.project import GuiProject, GuiProjectCreation
from accgtk.filefilters import FileFilters
from accgtk.threadcontrol import guithread, background
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib
from accgtk.exceptioncatcher import catch_exception


class ProjectEvents(Observable):

	def dummy_callback(self):
		pass

	def __init__(self, observer, strategy, elements, dropdown):
		super().__init__(observer, strategy, elements)
		self.dropdown = dropdown
		self.dropdown.extend_selection_callback(self.extended_selection_callback)
		self.project_directory = None
		self.last_open_callback_id = None

	def attach_events(self):
		self.elements.get_object("new_project_button").connect("clicked", self.new_project_callback)
		self.elements.get_object("save_project_button").connect("clicked", self.save_project)
		self.elements.get_object("remove_project_button").connect("clicked", self.on_delete_project)

	'''
	Direct callbacks
	'''

	@guithread
	def set_sensitivity(self, active: bool):
		self.elements.get_object("project_folder_open").set_sensitive(active)
		self.elements.get_object("remove_project_button").set_sensitive(active)
		self.elements.get_object("quote_selection_dropdown").set_sensitive(active)
		self.elements.get_object("invoice_selection_dropdown").set_sensitive(active)
		self.elements.get_object("reminder_selection_dropdown").set_sensitive(active)
		self.elements.get_object("expense_selection_dropdown").set_sensitive(active)
		self.elements.get_object("miscrecord_selection_dropdown").set_sensitive(active)
		self.elements.get_object("new_quote_button").set_sensitive(active)
		self.elements.get_object("new_invoice_button").set_sensitive(active)
		self.elements.get_object("new_reminder_button").set_sensitive(active)
		self.elements.get_object("new_expense_button").set_sensitive(active)
		self.elements.get_object("new_miscrecord_button").set_sensitive(active)
		self.elements.get_object("quote_form_container").set_sensitive(active)
		self.elements.get_object("invoice_form_container").set_sensitive(active)
		self.elements.get_object("reminder_form_container").set_sensitive(active)
		self.elements.get_object("expense_form_container").set_sensitive(active)
		self.elements.get_object("miscrecord_form_container").set_sensitive(active)
		if active:
			pass
		else:
			pass

	@guithread
	def focus_project(self):
		self.elements.get_object("project_form_notebook").set_current_page(1)

	@guithread
	def new_project_callback(self, *args):
		self.dropdown.set_property("selected", "0")
		self.focus_project()

	@guithread
	def attach_open_directory_callback(self):
		if self.last_open_callback_id is not None:
			self.elements.get_object("project_folder_open").disconnect(self.last_open_callback_id)
		if self.project_directory is not None:
			self.last_open_callback_id = self.elements.get_object("project_folder_open").connect("clicked", self.open_directory_callback)
		else:
			self.last_open_callback_id = None

	@guithread
	def on_delete_project(self, *args):
		unique = AccGtkHelpers.unique_or_none(self.dropdown.selected)
		if unique is not None:
			if self.observer.last_delete_callback_id is not None:
				self.elements.get_object("confirm_delete_yes").disconnect(self.observer.last_delete_callback_id)
			rm_project = self.strategy.get_project_by_unique(unique)
			self.observer.last_delete_callback_id = self.elements.get_object("confirm_delete_yes").connect("clicked", self.on_delete_project_confirmed, rm_project)
			self.elements.get_object("delete_confirm_dialog_label").set_text("Really delete project \"{}\"?".format(rm_project.name))
			self.elements.get_object("confirm_delete_dialog").show()

	@guithread
	def fill_subproject_dropdowns(self, payment_mode, expense_categoires, reminders, employees):
		self.elements.get_object("reminder_extended_by_dropdown").set_model(reminders)
		self.elements.get_object("invoice_extended_by_dropdown").set_model(reminders)
		self.elements.get_object("expense_advanced_by_dropdown").set_model(employees)
		self.elements.get_object("expense_payment_method_dropdown").set_model(payment_mode)
		self.elements.get_object("expense_category_dropdown").set_model(expense_categoires)

	def on_delete_project_confirmed(self, button, rm_project):
		self.delete_project(rm_project)

	'''
	Helper methods
	'''

	def extended_selection_callback(self):
		newid = AccGtkHelpers.unique_or_none(self.dropdown.selected)
		if newid is None or newid == "0":
			GuiProject.empty_form(self.elements)
			self.update_assets(None)
			self.set_sensitivity(False)
			self.project_directory = None
			self.prepare_subproject_dropdowns(None)
		else:
			p = self.strategy.get_project_by_unique(newid)
			guiproject = GuiProject.parse_obj(p)
			guiproject.to_gui(self.elements)
			self.project_directory = guiproject.yamlfile.parent
			self.update_assets(guiproject)
			self.set_sensitivity(True)
			self.focus_project()
			self.prepare_subproject_dropdowns(guiproject)
		self.attach_open_directory_callback()

	'''
	Background worker methods
	'''

	@background
	@catch_exception
	def open_directory_callback(self, *args):
		if self.project_directory is not None:
			AccGtkHelpers.fm_open_path(self.project_directory)

	@background
	@catch_exception
	def set_project_list(self, projects: Optional[list]):
		if projects is None:
			self.dropdown.update_dropdown_options([])
			self.dropdown.set_property("is_active", False)
		else:
			self.dropdown.update_dropdown_options(projects)
			self.dropdown.set_property("is_active", True)

	@background
	@catch_exception
	def save_project(self, *args):
		customer_id = AccGtkHelpers.unique_or_none(self.observer.get_customer_id())
		new_project = GuiProjectCreation.from_gui(self.elements, customer_id)
		new_project = self.strategy.save_project(new_project)
		projects = self.strategy.get_customer_by_unique(customer_id).projects
		self.set_project_list(projects=projects)
		self.dropdown.update_dropdown_selection(selected=new_project.unique)

	@background
	@catch_exception
	def delete_project(self, rm_project):
		customer_id = AccGtkHelpers.unique_or_none(self.observer.get_customer_id())
		GLib.idle_add(self.elements.get_object("confirm_delete_dialog").hide)
		self.strategy.delete_project(rm_project)
		projects = self.strategy.get_customer_by_unique(customer_id).projects
		self.set_project_list(projects=projects)
		self.dropdown.update_dropdown_selection(selected="0")

	@background
	@catch_exception
	def prepare_subproject_dropdowns(self, project: Optional[GuiProject]):
		employees = Gtk.ListStore(str, str)
		payment_mode = Gtk.ListStore(str, str)
		expense_categories = Gtk.ListStore(str, str)
		reminders = Gtk.ListStore(str, str)
		employees.append(["No one", "0"])
		reminders.append(["No reminder", "0"])
		expense_categories.append(["- not categorized -", ""])
		for i in PaymentMode:
			payment_mode.append([i.value, i.value])
		if project is not None:
			for i in project.reminders:
				reminders.append([i.name, str(i.unique)])
			empldata = self.strategy.get_employee_list()
			for i in empldata.__root__:
				employees.append([i.name, str(i.unique)])
			exp_cat = self.strategy.get_expense_categories()
			for i in exp_cat:
				expense_categories.append([i, i])
		self.fill_subproject_dropdowns(payment_mode, expense_categories, reminders, employees)

	@background
	@catch_exception
	def update_assets(self, project: Optional[GuiProject]):
		if project is None:
			self.observer.update_quotes(None)
			self.observer.update_invoices(None)
			self.observer.update_reminders(None)
			self.observer.update_expenses(None)
			self.observer.update_miscrecords(None)
		else:
			self.observer.update_quotes(project.quotes)
			self.observer.update_invoices(project.invoices)
			self.observer.update_reminders(project.reminders)
			self.observer.update_expenses(project.expenses)
			self.observer.update_miscrecords(project.miscrecords)
