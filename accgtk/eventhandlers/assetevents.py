import uuid
from typing import Optional
from accthymemodels.models.enums import PaymentMode
from accthymemodels.models.enums import SubprojectKeys
from accgtk.helpers import AccGtkHelpers
from accgtk.eventhandlers.interfaces import Observable
from accgtk.guimodels.invoice import GuiInvoice, GuiInvoiceCreation
from accgtk.guimodels.quote import GuiQuote, GuiQuoteCreation
from accgtk.guimodels.reminder import GuiReminder, GuiReminderCreation
from accgtk.guimodels.expense import GuiExpense, GuiExpenseCreation
from accgtk.guimodels.miscrecord import GuiMiscRecord, GuiMiscRecordCreation
from accgtk.filefilters import FileFilters
from accgtk.threadcontrol import guithread, background
from accgtk.eventhandlers.dialogevents import DialogEvents
from accgtk.filefilters import FileFilters
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib
from accgtk.exceptioncatcher import catch_exception


class AssetEvents(Observable):

	MODEL_TYPE_DICT = {
		SubprojectKeys.QUOTE: (GuiQuote, GuiQuoteCreation, "quotes", "quote"),
		SubprojectKeys.INVOICE: (GuiInvoice, GuiInvoiceCreation, "invoices", "invoice"),
		SubprojectKeys.REMINDER: (GuiReminder, GuiReminderCreation, "reminders", "reminder"),
		SubprojectKeys.EXPENSE: (GuiExpense, GuiExpenseCreation, "expenses", "expense"),
		SubprojectKeys.MISCRECORD: (GuiMiscRecord, GuiMiscRecordCreation, "miscrecords", "miscrecord")
	}

	def dummy_callback(self):
		pass

	def __init__(self, observer, strategy, elements, dropdown, typekey: SubprojectKeys):
		super().__init__(observer, strategy, elements)
		self.dropdown = dropdown
		self.typekey = typekey
		self.dropdown.extend_selection_callback(self.extended_selection_callback)

	def attach_events(self):
		callbacks = {
			SubprojectKeys.QUOTE: self.attach_quote_events,
			SubprojectKeys.INVOICE: self.attach_invoice_events,
			SubprojectKeys.REMINDER: self.attach_reminder_events,
			SubprojectKeys.EXPENSE: self.attach_expense_events,
			SubprojectKeys.MISCRECORD: self.attach_miscrecord_events
		}
		callbacks[self.typekey]()

	def attach_quote_events(self):
		self.elements.get_object("new_quote_button").connect("clicked", self.new_asset_callback)
		self.elements.get_object("save_quote_button").connect("clicked", self.save_asset)
		self.elements.get_object("remove_quote_button").connect("clicked", self.on_delete_asset)
		self.elements.get_object("quote_find_button").connect("clicked", self.on_path_pick, "quote_path_entry")
		self.elements.get_object("quote_find_clear_button").connect("clicked", self.on_field_clear, "quote_path_entry")
		self.elements.get_object("quote_sent_date_pick_button").connect("clicked", self.observer.show_calendar, "quote_sent_date_entry")
		self.elements.get_object("quote_sent_date_clear_button").connect("clicked", self.on_field_clear, "quote_sent_date_entry")

	def attach_invoice_events(self):
		self.elements.get_object("new_invoice_button").connect("clicked", self.new_asset_callback)
		self.elements.get_object("save_invoice_button").connect("clicked", self.save_asset)
		self.elements.get_object("remove_invoice_button").connect("clicked", self.on_delete_asset)
		self.elements.get_object("invoice_find_button").connect("clicked", self.on_path_pick, "invoice_path_entry")
		self.elements.get_object("invoice_find_clear_button").connect("clicked", self.on_field_clear, "invoice_path_entry")
		self.elements.get_object("invoice_sent_date_pick_button").connect("clicked", self.observer.show_calendar, "invoice_sent_date_entry")
		self.elements.get_object("invoice_sent_date_clear_button").connect("clicked", self.on_field_clear, "invoice_sent_date_entry")
		self.elements.get_object("invoice_date_of_settlement_pick_button").connect("clicked", self.observer.show_calendar, "invoice_date_of_settlement_entry")
		self.elements.get_object("invoice_date_of_settlement_clear_button").connect("clicked", self.on_field_clear, "invoice_date_of_settlement_entry")

	def attach_reminder_events(self):
		self.elements.get_object("new_reminder_button").connect("clicked", self.new_asset_callback)
		self.elements.get_object("save_reminder_button").connect("clicked", self.save_asset)
		self.elements.get_object("remove_reminder_button").connect("clicked", self.on_delete_asset)
		self.elements.get_object("reminder_find_button").connect("clicked", self.on_path_pick, "reminder_path_entry")
		self.elements.get_object("reminder_find_clear_button").connect("clicked", self.on_field_clear, "reminder_path_entry")
		self.elements.get_object("reminder_sent_date_pick_button").connect("clicked", self.observer.show_calendar, "reminder_sent_date_entry")
		self.elements.get_object("reminder_sent_date_clear_button").connect("clicked", self.on_field_clear, "reminder_sent_date_entry")

	def attach_expense_events(self):
		self.elements.get_object("new_expense_button").connect("clicked", self.new_asset_callback)
		self.elements.get_object("save_expense_button").connect("clicked", self.save_asset)
		self.elements.get_object("remove_expense_button").connect("clicked", self.on_delete_asset)
		self.elements.get_object("expense_find_button").connect("clicked", self.on_path_pick, "expense_path_entry")
		self.elements.get_object("expense_find_clear_button").connect("clicked", self.on_field_clear, "expense_path_entry")
		self.elements.get_object("expense_accural_date_pick_button").connect("clicked", self.observer.show_calendar, "expense_accural_date_entry")
		self.elements.get_object("expense_accural_date_clear_button").connect("clicked", self.on_field_clear, "expense_accural_date_entry")
		self.elements.get_object("expense_settlement_date_pick_button").connect("clicked", self.observer.show_calendar, "expense_settlement_date_entry")
		self.elements.get_object("expense_settlement_date_clear_button").connect("clicked", self.on_field_clear, "expense_settlement_date_entry")
		self.elements.get_object("expense_category_add_dialog").connect("delete_event", self.on_add_expense_category_hide)
		self.elements.get_object("expense_category_add_button").connect("clicked", self.on_add_expense_category)
		self.elements.get_object("new_expense_category_cancel").connect("clicked", self.on_add_expense_category_hide)
		self.elements.get_object("new_expense_category_ok").connect("clicked", self.on_add_expense_category_confirm)
		self.elements.get_object("expense_settlement_transaction_pick_button").connect("clicked", self.on_pick_transaction_dialog)

	def attach_miscrecord_events(self):
		self.elements.get_object("new_miscrecord_button").connect("clicked", self.new_asset_callback)
		self.elements.get_object("save_miscrecord_button").connect("clicked", self.save_asset)
		self.elements.get_object("remove_miscrecord_button").connect("clicked", self.on_delete_asset)
		self.elements.get_object("miscrecord_find_button").connect("clicked", self.on_path_pick, "miscrecord_path_entry")
		self.elements.get_object("miscrecord_find_clear_button").connect("clicked", self.on_field_clear, "miscrecord_path_entry")
		self.elements.get_object("miscrec_accural_date_pick_button").connect("clicked", self.observer.show_calendar, "miscrec_accural_date_entry")
		self.elements.get_object("miscrec_accural_date_clear_button").connect("clicked", self.on_field_clear, "miscrec_accural_date_entry")

	'''
	Direct callbacks
	'''

	@guithread
	def set_sensitivity(self, active: bool):
		rm_buttons = {
			SubprojectKeys.QUOTE: self.elements.get_object("remove_quote_button"),
			SubprojectKeys.INVOICE: self.elements.get_object("remove_invoice_button"),
			SubprojectKeys.REMINDER: self.elements.get_object("remove_reminder_button"),
			SubprojectKeys.EXPENSE: self.elements.get_object("remove_expense_button"),
			SubprojectKeys.MISCRECORD: self.elements.get_object("remove_miscrecord_button")
		}
		rm_buttons[self.typekey].set_sensitive(active)

	@guithread
	def focus_asset(self):
		assetpage = {
			SubprojectKeys.QUOTE: 2,
			SubprojectKeys.INVOICE: 3,
			SubprojectKeys.REMINDER: 4,
			SubprojectKeys.EXPENSE: 5,
			SubprojectKeys.MISCRECORD: 6
		}
		self.elements.get_object("project_form_notebook").set_current_page(assetpage[self.typekey])

	@guithread
	def new_asset_callback(self, *args):
		self.dropdown.set_property("selected", "0")
		self.focus_asset()

	@guithread
	def new_asset_callback(self, *args):
		self.dropdown.set_property("selected", "0")
		self.focus_asset()

	@guithread
	def on_delete_asset(self, *args):
		unique = AccGtkHelpers.unique_or_none(self.dropdown.selected)
		if unique is not None:
			if self.observer.last_delete_callback_id is not None:
				self.elements.get_object("confirm_delete_yes").disconnect(self.observer.last_delete_callback_id)
			rm_asset = self.strategy.get_asset_by_unique(unique)
			self.observer.last_delete_callback_id = self.elements.get_object("confirm_delete_yes").connect("clicked", self.on_delete_asset_confirmed, rm_asset)
			self.elements.get_object("delete_confirm_dialog_label").set_text(
				"Really remove {} \"{}\"?".format(self.MODEL_TYPE_DICT[self.typekey][3], rm_asset.name))
			self.elements.get_object("confirm_delete_dialog").show()

	@guithread
	def on_field_clear(self, button, entry_id):
		self.elements.get_object(entry_id).set_text("")

	@guithread
	def on_add_expense_category(self, *args):
		self.elements.get_object("new_expense_category_entry").set_text("")
		self.elements.get_object("expense_category_add_dialog").show()

	@guithread
	def on_add_expense_category_hide(self, *args):
		self.elements.get_object("expense_category_add_dialog").hide()
		return True

	@guithread
	def on_add_expense_category_confirm(self, *args):
		new_cat = self.elements.get_object("new_expense_category_entry").get_text()
		if new_cat != "":
			mod = self.elements.get_object("expense_category_dropdown").get_model()
			mod.append([new_cat, new_cat])
			self.elements.get_object("expense_category_dropdown").set_model(mod)
			self.elements.get_object("expense_category_dropdown").set_active_id(new_cat)
		self.on_add_expense_category_hide()

	@guithread
	@catch_exception
	def on_pick_transaction_dialog(self, *args):
		raise NotImplementedError("Feature is still missing")

	def on_path_pick(self, button, entry_id):
		project_unique = self.observer.get_project_id()
		project = self.strategy.get_project_by_unique(AccGtkHelpers.unique_or_none(project_unique))
		basedir = project.yamlfile.parent
		DialogEvents.file_chooser_dialog(self.elements, entry_id, str(basedir), "pdf")
		return True

	def on_delete_asset_confirmed(self, button, rm_asset):
		self.delete_asset(rm_asset)

	'''
	Helper methods
	'''

	def extended_selection_callback(self):
		objmodel = self.MODEL_TYPE_DICT[self.typekey][0]
		newid = AccGtkHelpers.unique_or_none(self.dropdown.selected)
		if newid is None or newid == "0":
			objmodel.empty_form(self.elements)
			self.set_sensitivity(False)
		else:
			a = self.strategy.get_asset_by_unique(newid)
			guiasset = objmodel.parse_obj(a)
			guiasset.to_gui(self.elements)
			self.set_sensitivity(True)
			self.focus_asset()

	'''
	Background worker methods
	'''

	@background
	@catch_exception
	def set_asset_list(self, assets: Optional[list]):
		if assets is None:
			self.dropdown.update_dropdown_options([])
			self.dropdown.set_property("is_active", False)
		else:
			self.dropdown.update_dropdown_options(assets)
			self.dropdown.set_property("is_active", True)

	@background
	@catch_exception
	def save_asset(self, *args):
		project_id = AccGtkHelpers.unique_or_none(self.observer.get_project_id())
		new_asset = self.MODEL_TYPE_DICT[self.typekey][1].from_gui(self.elements, project_id)
		new_asset = self.strategy.save_asset(new_asset)
		project = self.strategy.get_project_by_unique(project_id)
		asset_list = getattr(project, self.MODEL_TYPE_DICT[self.typekey][2])
		self.set_asset_list(assets=asset_list)
		self.dropdown.update_dropdown_selection(selected=new_asset.unique)

	@background
	@catch_exception
	def delete_asset(self, rm_asset):
		project_id = AccGtkHelpers.unique_or_none(self.observer.get_project_id())
		GLib.idle_add(self.elements.get_object("confirm_delete_dialog").hide)
		self.strategy.delete_asset(rm_asset)
		project = self.strategy.get_project_by_unique(project_id)
		asset_list = getattr(project, self.MODEL_TYPE_DICT[self.typekey][2])
		self.set_asset_list(assets=asset_list)
		self.dropdown.update_dropdown_selection(selected="0")
