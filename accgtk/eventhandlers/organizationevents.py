import uuid
from typing import Optional
from accgtk.helpers import AccGtkHelpers
from accgtk.eventhandlers.interfaces import Observable
from accgtk.guimodels.organization import GuiOrganization
from accgtk.filefilters import FileFilters
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from accgtk.threadcontrol import guithread, background
from accgtk.eventhandlers.dialogevents import DialogEvents
from accgtk.exceptioncatcher import catch_exception


class OrganizationEvents(Observable):

	def attach_events(self):
		self.elements.get_object("save_organization_form_button").connect("clicked", self.save_organization)
		self.elements.get_object("organization_logo_pick_button").connect("clicked", self.on_path_pick, "organization_logo_entry")
		self.elements.get_object("organization_logo_clear_button").connect("clicked", self.on_field_clear, "organization_logo_entry")
		self.elements.get_object("refresh_button_organization").connect("clicked", self.load_organization)

	@guithread
	def load_organization(self, *args):
		if self.strategy is not None:
			org = GuiOrganization.parse_obj(self.strategy.get_organization())
			org.to_gui(self.elements)

	@guithread
	def clear_form(self):
		GuiOrganization.empty_form(self.elements)

	@background
	@catch_exception
	def save_organization(self, *args):
		if self.strategy is not None:
			org = GuiOrganization.from_gui(self.elements)
			self.strategy.save_organization(org)
			self.load_organization()

	@guithread
	def on_field_clear(self, button, entry_id):
		self.elements.get_object(entry_id).set_text("")

	def on_path_pick(self, button, entry_id):
		basedir = self.strategy.cpeobj.config.acc.basepath
		DialogEvents.file_chooser_dialog(self.elements, entry_id, str(basedir), "img")
		return True
