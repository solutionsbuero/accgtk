import uuid
from typing import Optional
from accgtk.helpers import AccGtkHelpers
from accgtk.eventhandlers.interfaces import Observable
from accgtk.guimodels.config import GuiConfig
from accgtk.filefilters import FileFilters
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib
from accgtk.threadcontrol import guithread, background
from accgtk.messages import GitStatus
from accgtk.exceptioncatcher import catch_exception


class GitEvents(Observable):

	def attach_events(self):
		self.elements.get_object("refresh_button_git").connect("clicked", self.check_status)
		self.elements.get_object("git_rollback_button").connect("clicked", self.git_rollback)
		self.elements.get_object("git_pull_button").connect("clicked", self.git_pull)
		self.elements.get_object("git_commit_push_button").connect("clicked", self.git_push)

	@guithread
	def set_status(self, v: Optional[GitStatus]):
		if v is None:
			self.elements.get_object("git_status_label").set_text("")
		else:
			self.elements.get_object("git_status_label").set_text(v.value)

	@guithread
	def set_sensitivity(self, v: Optional[GitStatus]):
		if v is None or v == GitStatus.DISABLED:
			self.elements.get_object("git_rollback_button").set_sensitive(False)
			self.elements.get_object("git_pull_button").set_sensitive(False)
			self.elements.get_object("git_commit_push_button").set_sensitive(False)
		else:
			self.elements.get_object("git_rollback_button").set_sensitive(True)
			self.elements.get_object("git_pull_button").set_sensitive(True)
			self.elements.get_object("git_commit_push_button").set_sensitive(False)
			if v == GitStatus.DIRTY:
				self.elements.get_object("git_commit_push_button").set_sensitive(True)

	@background
	@catch_exception
	def check_status(self, *args):
		if self.strategy is None:
			msg = None
		else:
			if self.strategy.check_repo():
				msg = GitStatus.DIRTY if self.strategy.git_is_dirty() else GitStatus.DIRTY
			else:
				msg = GitStatus.DISABLED
		self.set_status(msg)
		self.set_sensitivity(msg)

	@background
	@catch_exception
	def git_pull(self, *args):
		self.strategy.git_update_local()
		self.check_status()

	@background
	@catch_exception
	def git_push(self, *args):
		self.strategy.git_update_remote()
		self.check_status()

	@guithread
	def git_rollback(self, *args):
		if self.observer.last_delete_callback_id is not None:
			self.elements.get_object("confirm_delete_yes").disconnect(self.observer.last_delete_callback_id)
		self.observer.last_delete_callback_id = self.elements.get_object("confirm_delete_yes").connect("clicked", self.git_rollback_confirmed)
		self.elements.get_object("delete_confirm_dialog_label").set_text("Really rollback all changes?")
		self.elements.get_object("confirm_delete_dialog").show()

	@background
	@catch_exception
	def git_rollback_confirmed(self, *args):
		GLib.idle_add(self.elements.get_object("confirm_delete_dialog").hide)
		self.strategy.git_clean()
		self.check_status()
		return True
