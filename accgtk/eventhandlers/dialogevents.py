import uuid
import datetime
from typing import Optional
from accgtk.helpers import AccGtkHelpers
from accgtk.eventhandlers.interfaces import Observable
from accgtk.guimodels.config import GuiConfig
from accgtk.filefilters import FileFilters
from accgtk.threadcontrol import guithread
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib


class DialogEvents(Observable):

	def __init__(self, observer, strategy, elements):
		super().__init__(observer, strategy, elements)
		# self.last_calendar_callback = None
		self.calendar_target_entry = None

	def attach_events(self):
		self.elements.get_object("confirm_delete_no").connect("clicked", self.cancel_delete_confirm)
		self.elements.get_object("confirm_delete_dialog").connect("delete_event", self.cancel_delete_confirm)
		self.elements.get_object("calendar_dialog").connect("delete_event", self.hide_calendar_dialog)
		self.elements.get_object("calendar_dialog_cancel").connect("clicked", self.hide_calendar_dialog)
		self.elements.get_object("calendar_dialog_ok").connect("clicked", self.on_calendar_picked)

	@staticmethod
	def file_chooser_dialog(elements, return_entry_id: str, init_dir: Optional[str] = None, filtername: str = "all", title: str = "Choose a file"):
		window = elements.get_object("mainwindow")
		dialog = Gtk.FileChooserDialog(title, window, Gtk.FileChooserAction.OPEN, (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
		FileFilters.apply_filter(filtername, dialog)
		if init_dir is not None:
			dialog.set_current_folder(init_dir)
		response = dialog.run()
		if response == Gtk.ResponseType.OK:
			return_entry = elements.get_object(return_entry_id)
			filename = dialog.get_filename()
			return_entry.set_text(filename)
		dialog.destroy()
		return True

	@staticmethod
	def directory_chooser_dialog(elements, return_entry_id, title: str = "Choose a directory"):
		window = elements.get_object("mainwindow")
		dialog = Gtk.FileChooserDialog(title, window, Gtk.FileChooserAction.SELECT_FOLDER,	(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
		response = dialog.run()
		if response == Gtk.ResponseType.OK:
			return_entry = elements.get_object(return_entry_id)
			filename = dialog.get_filename()
			return_entry.set_text(filename)
		dialog.destroy()

	def cancel_delete_confirm(self, *args):
		GLib.idle_add(self.elements.get_object("confirm_delete_dialog").hide)
		return True

	def hide_calendar_dialog(self, *args):
		GLib.idle_add(self.elements.get_object("calendar_dialog").hide)
		return True

	@guithread
	def on_calendar_picked(self, *args):
		if self.calendar_target_entry is not None:
			picked = self.elements.get_object("dialog_calendar").get_date()
			picked = datetime.date(year=picked[0], month=(picked[1] + 1), day=picked[2])
			self.calendar_target_entry.set_text(picked.isoformat())
		self.calendar_target_entry = None
		self.hide_calendar_dialog()

	@guithread
	def show_calendar(self, return_id):
		self.calendar_target_entry = self.elements.get_object(return_id)
		try:
			preset = datetime.date.fromisoformat(self.calendar_target_entry.get_text())
		except (ValueError, AttributeError):
			preset = datetime.date.today()
		self.elements.get_object("dialog_calendar").select_month(preset.month - 1, preset.year)
		self.elements.get_object("dialog_calendar").select_day(preset.day)
		self.elements.get_object("calendar_dialog").show()
