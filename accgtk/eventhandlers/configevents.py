import time
import uuid
import threading
from typing import Optional
from accgtk.helpers import AccGtkHelpers
from accgtk.threadcontrol import background, guithread
from accgtk.messages import FeedbackMessages
from accgtk.eventhandlers.interfaces import Observable
from accgtk.guimodels.config import GuiConfig
from accgtk.filefilters import FileFilters
from accgtk.eventhandlers.dialogevents import DialogEvents
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GLib
from time import sleep
from accgtk.exceptioncatcher import catch_exception


class ConfigEvents(Observable):

	def __init__(self, observer, strategy, elements, dropdown):
		super().__init__(observer, strategy, elements)
		self.dropdown = dropdown
		self.dropdown.extend_selection_callback(self.extended_selection_callback)

	def attach_events(self):
		self.elements.get_object("config_edit_button").connect("clicked", self.on_show_org_config_dialog)
		self.elements.get_object("new_organization_button").connect("clicked", self.on_show_org_config_dialog)
		self.elements.get_object("config_cancel_button").connect("clicked", self.on_cancel_org_config_dialog)
		self.elements.get_object("config_find_button").connect("clicked", self.on_org_config_path_pick)
		self.elements.get_object("config_save_button").connect("clicked", self.on_save_org_config_dialog)
		self.elements.get_object("remove_organization_button").connect("clicked", self.on_delete_org_conf)
		# self.elements.get_object("organization_dropdown").connect("changed", self.on_org_config_changed)  # Obsolete, don't re-attach!
		# Just hide, don't destroy addtional dialogs on close
		self.elements.get_object("config_modify_dialog").connect("delete_event", self.on_cancel_org_config_dialog)

	'''
	Direct callbacks
	'''

	def on_org_config_path_pick(self, *args):
		DialogEvents.directory_chooser_dialog(elements=self.elements, return_entry_id="config_path_entry", title="Choose acc base directory")

	def on_show_org_config_dialog(self, *args):
		self.elements.get_object("status_label").set_text(FeedbackMessages.LOADING)
		if Gtk.Buildable.get_name(args[0]) == "config_edit_button":
			self.fill_org_config_dialog()
		else:
			self.fill_org_config_dialog(empty=True)
		dialog = self.elements.get_object("config_modify_dialog")
		self.elements.get_object("status_label").set_text(FeedbackMessages.READY)
		dialog.show_all()
		return True

	def on_save_org_config_dialog(self, *args):
		self.save_org_config()
		self.list_org_configs()

	def on_delete_org_conf(self, *args):
		unique = AccGtkHelpers.unique_or_none(self.elements.get_object("organization_dropdown").get_active_id())
		if unique is not None:
			if self.observer.last_delete_callback_id is not None:
				self.elements.get_object("confirm_delete_yes").disconnect(self.observer.last_delete_callback_id)
			config_to_remove = GuiConfig.get_config_by_unique(unique)
			self.observer.last_delete_callback_id = self.elements.get_object("confirm_delete_yes").connect("clicked", self.on_confirmed_delete_org_config, config_to_remove)
			self.elements.get_object("delete_confirm_dialog_label").set_text("Really remove configuration for {}?".format(config_to_remove.name))
			self.elements.get_object("confirm_delete_dialog").show()

	def on_confirmed_delete_org_config(self, button, config):
		self.delete_org_config(config)
		self.list_org_configs()

	def on_cancel_org_config_dialog(self, *args):
		GLib.idle_add(self.elements.get_object("config_modify_dialog").hide)
		return True

	@guithread
	def set_sensitivity(self, active: bool):
		self.elements.get_object("remove_organization_button").set_sensitive(active)
		self.elements.get_object("config_edit_button").set_sensitive(active)
		self.elements.get_object("new_customer_button").set_sensitive(active)

	'''
	Helper methods
	'''

	def fill_org_config_dialog(self, empty=False):
		unique = AccGtkHelpers.unique_or_none(self.elements.get_object("organization_dropdown").get_active_id())
		if unique is None or empty:
			GuiConfig.empty_form(self.elements)
		else:
			config = GuiConfig.get_config_by_unique(unique)
			config.to_gui(self.elements)

	def extended_selection_callback(self):
		newid = self.dropdown.selected
		if newid is None or newid == "0":
			self.set_sensitivity(False)
		else:
			self.set_sensitivity(True)
		self.initialize_model(newid)

	'''
	Background worker methods
	'''

	@background
	@catch_exception
	def list_org_configs(self):
		self.observer.set_status_message(FeedbackMessages.LOADING)
		org_conf_list = GuiConfig.get_config_list()
		self.dropdown.update_dropdown_options(options=org_conf_list)
		self.observer.set_status_message(FeedbackMessages.READY)

	@background
	@catch_exception
	def save_org_config(self):
		self.observer.set_status_message(FeedbackMessages.PROCESSING)
		self.on_cancel_org_config_dialog()
		config = GuiConfig.from_gui(self.elements)
		newconfig = config.save()
		org_conf_list = GuiConfig.get_config_list()
		self.dropdown.update_dropdown_options(options=org_conf_list)
		self.dropdown.update_dropdown_selection(selected=newconfig.unique)
		self.observer.set_status_message(FeedbackMessages.READY)

	@background
	@catch_exception
	def delete_org_config(self, config):
		self.elements.get_object("status_label").set_text(FeedbackMessages.PROCESSING)
		GLib.idle_add(self.elements.get_object("confirm_delete_dialog").hide)
		config.delete()
		org_conf_list = GuiConfig.get_config_list()
		self.dropdown.update_dropdown_options(options=org_conf_list)
		self.dropdown.update_dropdown_selection(selected="0")
		self.observer.set_status_message(FeedbackMessages.READY)

	@background
	@catch_exception
	def initialize_model(self, configid: str):
		new_config = GuiConfig.get_config_by_unique(configid)
		self.observer.init_model(new_config)
