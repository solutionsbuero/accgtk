import uuid
from typing import Optional
from accthymemodels.models.enums import PaymentMode
from accgtk.helpers import AccGtkHelpers
from accgtk.eventhandlers.interfaces import Observable
from accgtk.guimodels.employee import GuiEmployee, GuiEmployeeCreation
from accgtk.filefilters import FileFilters
from accgtk.threadcontrol import guithread, background
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib
from accgtk.exceptioncatcher import catch_exception


class EmployeeEvents(Observable):

	def dummy_callback(self):
		pass

	def __init__(self, observer, strategy, elements, dropdown):
		super().__init__(observer, strategy, elements)
		self.dropdown = dropdown
		self.dropdown.extend_selection_callback(self.extended_selection_callback)

	def attach_events(self):
		self.elements.get_object("new_employee_button").connect("clicked", self.new_employee_callback)
		self.elements.get_object("save_employee_button").connect("clicked", self.save_employee)
		self.elements.get_object("remove_employee_button").connect("clicked", self.on_delete_employee)
		self.elements.get_object("refresh_button_employees").connect("clicked", self.on_forced_update)

	'''
	Direct callbacks
	'''

	@guithread
	def set_sensitivity(self, active: bool):
		self.elements.get_object("remove_employee_button").set_sensitive(active)

	@guithread
	def new_employee_callback(self, *args):
		self.dropdown.set_property("selected", "0")

	@guithread
	def on_delete_employee(self, *args):
		unique = AccGtkHelpers.unique_or_none(self.dropdown.selected)
		if unique is not None:
			if self.observer.last_delete_callback_id is not None:
				self.elements.get_object("confirm_delete_yes").disconnect(self.observer.last_delete_callback_id)
			rm_employee = self.strategy.get_employee_by_unique(unique)
			self.observer.last_delete_callback_id = self.elements.get_object("confirm_delete_yes").connect("clicked", self.on_delete_employee_confirmed, rm_employee)
			self.elements.get_object("delete_confirm_dialog_label").set_text("Really remove employee \"{}\"?".format(rm_employee.name))
			self.elements.get_object("confirm_delete_dialog").show()

	def on_delete_employee_confirmed(self, button, rm_employee):
		self.delete_employee(rm_employee)

	'''
	Helper methods
	'''

	def update_model(self, model):
		self.strategy = model
		if self.strategy is None:
			self.clear_employee_list()
		else:
			elist = self.strategy.get_employee_list()
			self.set_employee_list(employees=elist.__root__)

	def extended_selection_callback(self):
		newid = AccGtkHelpers.unique_or_none(self.dropdown.selected)
		if newid is None or newid == "0":
			GuiEmployee.empty_form(self.elements)
			self.set_sensitivity(False)
		else:
			e = self.strategy.get_employee_by_unique(newid)
			guiemployee = GuiEmployee.parse_obj(e)
			guiemployee.to_gui(self.elements)
			self.set_sensitivity(True)

	'''
	Background worker methods
	'''

	@background
	@catch_exception
	def set_employee_list(self, employees: Optional[list]):
		if employees is None:
			self.dropdown.update_dropdown_options([])
			self.dropdown.set_property("is_active", False)
		else:
			self.dropdown.update_dropdown_options(employees)
			self.dropdown.set_property("is_active", True)

	@background
	@catch_exception
	def clear_employee_list(self):
		self.dropdown.update_dropdown_options([])
		self.dropdown.set_property("is_active", False)

	@background
	@catch_exception
	def save_employee(self, *args):
		new_employee = GuiEmployeeCreation.from_gui(self.elements)
		self.strategy.save_employee(new_employee)
		elist = self.strategy.get_employee_list()
		self.set_employee_list(employees=elist.__root__)
		self.dropdown.update_dropdown_selection(selected=new_employee.unique)

	@background
	@catch_exception
	def delete_employee(self, rm_employee):
		GLib.idle_add(self.elements.get_object("confirm_delete_dialog").hide)
		self.strategy.delete_employee(rm_employee)
		elist = self.strategy.get_employee_list()
		self.set_employee_list(employees=elist.__root__)
		self.dropdown.update_dropdown_selection(selected="0")

	@background
	@catch_exception
	def on_forced_update(self, *args):
		if self.strategy is not None:
			employees = self.strategy.get_employee_list(force_update=True)
			self.set_employee_list(employees.__root__)
			self.dropdown.update_dropdown_selection("0")
