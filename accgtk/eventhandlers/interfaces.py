from abc import ABC, abstractmethod


class Observable(ABC):

	def __init__(self, observer, strategy, elements):
		self.observer = observer
		self.strategy = strategy
		self.elements = elements
		self.window = elements.get_object("mainwindow")

	@abstractmethod
	def attach_events(self):
		pass

	def update_model(self, model):
		self.strategy = model
