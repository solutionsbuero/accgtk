import uuid
from typing import Optional
from accgtk.helpers import AccGtkHelpers
from accgtk.eventhandlers.interfaces import Observable
from accgtk.guimodels.config import GuiConfig
from accgtk.filefilters import FileFilters
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from accgtk.threadcontrol import guithread, background


class StatusEvents(Observable):

	def attach_events(self):
		self.elements.get_object("functionality_button_projects").connect("clicked", self.on_functionality_buttons, 0)
		self.elements.get_object("functionality_button_employees").connect("clicked", self.on_functionality_buttons, 1)
		self.elements.get_object("functionality_button_organization").connect("clicked", self.on_functionality_buttons, 2)
		self.elements.get_object("functionality_button_git").connect("clicked", self.on_functionality_buttons, 3)
		self.elements.get_object("functionality_button_report").connect("clicked", self.on_functionality_buttons, 4)

	@guithread
	def set_status_message(self, message):
		self.elements.get_object("status_label").set_text(message)

	@guithread
	def activate_main_controls(self, active: bool):
		self.elements.get_object("functionality_notebook").set_sensitive(active)

	@guithread
	def on_functionality_buttons(self, button, pageno: int):
		self.elements.get_object("functionality_notebook").set_current_page(pageno)
