from accgtk.view import AccView
from types import SimpleNamespace
from typing import Optional
import accthymemodels
from accthymemodels.models.enums import SubprojectKeys
from accgtk.messages import FeedbackMessages
from accgtk.guimodels.config import GuiConfig
from accgtk.eventhandlers.dialogevents import DialogEvents
from accgtk.eventhandlers.configevents import ConfigEvents
from accgtk.eventhandlers.customerevents import CustomerEvents
from accgtk.eventhandlers.statusevents import StatusEvents
from accgtk.eventhandlers.projectevents import ProjectEvents
from accgtk.representations.dropdown import RecordDropdown
from accgtk.eventhandlers.assetevents import AssetEvents
from accgtk.eventhandlers.employeeevents import EmployeeEvents
from accgtk.eventhandlers.organizationevents import OrganizationEvents
from accgtk.eventhandlers.exportevents import ExportEvents
from accgtk.exceptioncatcher import ExceptionCatcher, catch_exception
from accgtk.eventhandlers.gitevents import GitEvents


class AccPresenter:

	def __init__(self):
		self.view = AccView()
		self.catcher = ExceptionCatcher()
		self.catcher.set_elements(self.view.get_elements())
		self.model = None
		self.last_delete_callback_id = None
		self.representations = SimpleNamespace(
			config_dropdown=RecordDropdown(gui_element=self.view.get_element("organization_dropdown")),
			customer_dropdown=RecordDropdown(gui_element=self.view.get_element("customer_selection_dropdown")),
			project_dropdown=RecordDropdown(gui_element=self.view.get_element("project_selection_dropdown")),
			quote_dropdown=RecordDropdown(gui_element=self.view.get_element("quote_selection_dropdown")),
			invoice_dropdown=RecordDropdown(gui_element=self.view.get_element("invoice_selection_dropdown")),
			reminder_dropdown=RecordDropdown(gui_element=self.view.get_element("reminder_selection_dropdown")),
			expense_dropdown=RecordDropdown(gui_element=self.view.get_element("expense_selection_dropdown")),
			miscrecord_dropdown=RecordDropdown(gui_element=self.view.get_element("miscrecord_selection_dropdown")),
			employee_dropdown=RecordDropdown(gui_element=self.view.get_element("employee_selection_dropdown"))
		)
		self.observables = {
			"status_events": StatusEvents(observer=self, strategy=self.model, elements=self.view.get_elements()),
			"dialog_events": DialogEvents(observer=self, strategy=self.model, elements=self.view.get_elements()),
			"config_events": ConfigEvents(observer=self, strategy=self.model, elements=self.view.get_elements(), dropdown=self.representations.config_dropdown),
			"customer_events": CustomerEvents(observer=self, strategy=self.model, elements=self.view.get_elements(), dropdown=self.representations.customer_dropdown),
			"project_events": ProjectEvents(observer=self, strategy=self.model, elements=self.view.get_elements(), dropdown=self.representations.project_dropdown),
			"quote_events": AssetEvents(observer=self, strategy=self.model, elements=self.view.get_elements(), dropdown=self.representations.quote_dropdown, typekey=SubprojectKeys.QUOTE),
			"invoice_events": AssetEvents(observer=self, strategy=self.model, elements=self.view.get_elements(), dropdown=self.representations.invoice_dropdown, typekey=SubprojectKeys.INVOICE),
			"reminder_events": AssetEvents(observer=self, strategy=self.model, elements=self.view.get_elements(), dropdown=self.representations.reminder_dropdown, typekey=SubprojectKeys.REMINDER),
			"expense_events": AssetEvents(observer=self, strategy=self.model, elements=self.view.get_elements(), dropdown=self.representations.expense_dropdown, typekey=SubprojectKeys.EXPENSE),
			"miscrecord_events": AssetEvents(observer=self, strategy=self.model, elements=self.view.get_elements(), dropdown=self.representations.miscrecord_dropdown, typekey=SubprojectKeys.MISCRECORD),
			"employee_events": EmployeeEvents(observer=self, strategy=self.model, elements=self.view.get_elements(), dropdown=self.representations.employee_dropdown),
			"organization_events": OrganizationEvents(observer=self, strategy=self.model, elements=self.view.get_elements()),
			"git_events": GitEvents(observer=self, strategy=self.model, elements=self.view.get_elements()),
			"export_events": ExportEvents(observer=self, strategy=self.model, elements=self.view.get_elements())
		}
		for i in self.observables.values():
			i.attach_events()

	def show_view(self):
		self.activate_main_controls(False)
		self.observables["config_events"].list_org_configs()
		self.view.show_main()

	@catch_exception
	def init_model(self, config: Optional[GuiConfig]):
		if config is None:
			self.model = None
			self.activate_main_controls(False)
		else:
			self.model = accthymemodels.Dispatcher(config=config)
			self.activate_main_controls(True)
		for i in self.observables.values():
			i.update_model(self.model)
		if config is None:
			self.set_status_message(FeedbackMessages.NO_ORG_CONFIG)
			self.observables["organization_events"].clear_form()
		else:
			self.set_status_message(FeedbackMessages.READY)
			self.observables["organization_events"].load_organization()
		self.observables["git_events"].check_status()

	def activate_main_controls(self, active: bool):
		self.observables["status_events"].activate_main_controls(active)

	def set_status_message(self, message):
		self.observables["status_events"].set_status_message(message)

	'''
	Pass around uuids for object creation
	'''

	def get_customer_id(self):
		return self.observables["customer_events"].dropdown.selected

	def get_project_id(self):
		return self.observables["project_events"].dropdown.selected

	'''
	Update various dropdowns
	'''

	def update_projects(self, projects: Optional[list]):
		self.observables["project_events"].set_project_list(projects)

	def update_quotes(self, quotes: Optional[list]):
		self.observables["quote_events"].set_asset_list(quotes)

	def update_invoices(self, invoices: Optional[list]):
		self.observables["invoice_events"].set_asset_list(invoices)

	def update_reminders(self, reminders: Optional[list]):
		self.observables["reminder_events"].set_asset_list(reminders)

	def update_expenses(self, expenses: Optional[list]):
		self.observables["expense_events"].set_asset_list(expenses)

	def update_miscrecords(self, miscrecords: Optional[list]):
		self.observables["miscrecord_events"].set_asset_list(miscrecords)

	'''
	Show various dialogs
	'''

	def show_calendar(self, button, return_id):
		self.observables["dialog_events"].show_calendar(return_id)

	'''
	Force reloads
	'''
