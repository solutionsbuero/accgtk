from accgtk.presenter import AccPresenter


class AccGTK:

	@staticmethod
	def launch():
		pres = AccPresenter()
		pres.show_view()


if __name__ == "__main__":
	AccGTK.launch()
