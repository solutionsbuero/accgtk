# check if run as root
if [ "$EUID" -ne 0 ]
    then >&2 echo "ERROR: Please run me as root"
    exit
fi

# Determine distribution
echo "Uninstalling accgtk..."

rm /usr/share/applications/accgtk.desktop
rm -rf /opt/accgtk

echo "Done!"
