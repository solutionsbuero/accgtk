# check if run as root
if [ "$EUID" -ne 0 ]
    then >&2 echo "ERROR: Please run me as root"
    exit
fi

# Determine distribution
echo "Updating accgtk..."

. /opt/accgtk/venv/bin/activate
pip3 install git+https://gitlab.com/solutionsbuero/accgtk.git

echo "Done!"
