from enum import Enum
from dataclasses import dataclass


@dataclass(frozen=True)
class FeedbackMessages:
	READY = "READY"
	LOADING = "LOADING"
	PROCESSING = "PROCESSING"
	NO_ORG_CONFIG = "NO ORGANIZATION LOADED"


class GitStatus(Enum):
	DISABLED = "NOT A REPOSITORY"
	CLEAN = "CLEAN"
	DIRTY = "DIRTY"
