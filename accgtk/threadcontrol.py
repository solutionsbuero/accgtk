import threading
from functools import wraps
from accthymemodels.meta import Singleton
from gi.repository import GLib


def background(f):
	@wraps(f)
	def process(*args, **kwargs):
		processor = BackgroundProcessor()
		new_thread = threading.Thread(target=processor.run_task_blocking, args=[f, *args], kwargs=kwargs)
		new_thread.start()
	return process


def guithread(f):
	@wraps(f)
	def process(*args, **kwargs):
		GLib.idle_add(f, *args, **kwargs)
	return process


class BackgroundProcessor(metaclass=Singleton):

	def __init__(self):
		self.semaphore = threading.Semaphore()

	def run_task_blocking(self, f, *args, **kwargs):
		self.semaphore.acquire()
		f(*args, **kwargs)
		self.semaphore.release()

	def run_task_discarding(self, f, *args, **kwargs):
		unlocked = self.semaphore.acquire(blocking=False)
		if unlocked:
			f(*args, **kwargs)
			self.semaphore.release()
		else:
			print("lock is closed, discarding thread")
