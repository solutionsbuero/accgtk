import uuid
import os
import sys
import datetime
import subprocess
from iso4217 import Currency
from typing import Optional, Union
from pathlib import Path


class AccGtkHelpers:

	EMPTY_VALUES = ['0', '', 0, None]

	@staticmethod
	def empty_to_none(v):
		if v in AccGtkHelpers.EMPTY_VALUES:
			return None
		return v

	@staticmethod
	def none_to_empty(v):
		if v is None:
			return ""
		return v

	@staticmethod
	def datestr(v):
		if v is None:
			return ""
		else:
			return v.isoformat()

	@staticmethod
	def unique_or_none(v) -> Optional[uuid.UUID]:
		if v is None:
			return v
		elif isinstance(v, uuid.UUID):
			return v
		try:
			return uuid.UUID(v)
		except (ValueError, AttributeError, TypeError):
			return None

	@staticmethod
	def unique_to_str(v):
		if v == "0":
			return None
		else:
			return str(v)

	@staticmethod
	def unique_or_zero(v) -> Union[uuid.UUID, str]:
		v = AccGtkHelpers.unique_or_none(v)
		if v is None:
			return "0"
		return v

	@staticmethod
	def path_to_str(v):
		if v is None:
			return ""
		return str(v)

	@staticmethod
	def uuid_compare(c1, c2):
		if isinstance(c1, uuid.UUID):
			c1 = str(c1)
		if isinstance(c2, uuid.UUID):
			c2 = str(c2)
		return c1 == c2

	@staticmethod
	def text_to_date(v):
		if v in AccGtkHelpers.EMPTY_VALUES:
			return None
		try:
			datev = datetime.date.fromisoformat(v)
			return datev
		except ValueError:
			return None

	@staticmethod
	def currency_from_dropdown(v):
		if v in AccGtkHelpers.EMPTY_VALUES:
			return None
		else:
			return Currency(v)

	@staticmethod
	def fm_open_path(d: Path):
		if sys.platform == 'win32':
			subprocess.Popen(['start', d], shell=True)
		elif sys.platform == 'darwin':
			subprocess.Popen(['open', d])
		else:
			try:
				subprocess.Popen(['xdg-open', d])
			except OSError:
				pass
