import uuid

from accthymemodels.models.accthymeconfig import AccThymeConfig
from accthymemodels.mediator import DataMediator
from accthymemodels.accstrategy import AccStrategy
from accgtk.guimodels.config import GuiConfig

fulldict = GuiConfig.get_config_dict()
mediator = DataMediator(fulldict.get(uuid.UUID("259e4cd4-a69b-11eb-a3c9-4d48f6b27905")))
print(mediator)
strat = AccStrategy(mediator)
invoices = strat.get_ordered_invoices(2020)
for i in invoices:
	print(i.yaml())
expenses = strat.get_ordered_expenses(2020)
for i in expenses:
	print(i.yaml())
