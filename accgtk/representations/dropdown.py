import uuid
from types import FunctionType, MethodType
from typing import Union, Optional
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import GObject
from accgtk.helpers import AccGtkHelpers
from accgtk.threadcontrol import guithread, background


class RecordDropdown(GObject.Object):

	def empty_callback(self):
		return

	def __init__(self, gui_element):
		GObject.Object.__init__(self)
		self.gui_element = gui_element
		self.option_list: list = []
		self.combo_list = Gtk.ListStore(str, str)
		self.id_list: list = []
		self.selected_id = None
		self._extended_callback = self.empty_callback
		self._is_active = True
		self.connect("notify::selected", self._on_dropdown_selected)
		self.connect("notify::options", self._on_options_updated)
		self.connect("notify::is_active", self._on_change_active)
		self.gui_element.connect("changed", self._on_gui_changed)

	@GObject.Property()
	def options(self):
		return self.option_list

	@options.setter
	def set_options(self, values: list):
		self.option_list = []
		self.id_list = []
		self.combo_list = Gtk.ListStore(str, str)
		self.option_list.append(("0", "- new entry -"))
		self.combo_list.append(["- new entry -", "0"])
		self.id_list.append("0")
		for i in values:
			self.option_list.append((i.unique, i.name))
			self.combo_list.append([i.name, str(i.unique)])
			self.id_list.append(i.unique)

	@GObject.Property(type=str)
	def selected(self):
		return self.selected_id

	@selected.setter
	def set_selected(self, v: str):
		self.selected_id = v

	@GObject.Property()
	def is_active(self):
		return self._is_active

	@is_active.setter
	def set_active(self, active: bool):
		self._is_active = active

	def extend_selection_callback(self, f: Union[FunctionType, MethodType]):
		self._extended_callback = f

	@background
	def update_dropdown_options(self, options: list):
		self.set_property("options", options)

	@guithread
	def _on_options_updated(self, *args):
		self.gui_element.set_model(self.combo_list)
		preselected = AccGtkHelpers.unique_or_zero(self.selected_id)
		if preselected not in self.id_list:
			preselected = "0"
		self.update_dropdown_selection(preselected)

	@background
	def update_dropdown_selection(self, selected: Union[uuid.UUID, str]):
		self.set_property("selected", AccGtkHelpers.unique_or_zero(selected))

	@guithread
	def _on_dropdown_selected(self, *args):
		self.gui_element.set_active_id(str(self.get_property("selected")))
		self._extended_callback()

	@guithread
	def _on_change_active(self, *args):
		print("on change called")
		self.gui_element.set_sensitive(self.get_property("is_active"))

	@guithread
	def _on_gui_changed(self, *args):
		selected = self.gui_element.get_active_id()
		previous = self.get_property("selected")
		if not AccGtkHelpers.uuid_compare(selected, previous):
			self.update_dropdown_selection(selected)
