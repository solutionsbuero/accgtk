from accthymemodels.models.invoice import Invoice, InvoiceCreation
from accgtk.guimodels.interfaces import Loadable, Savable
from accgtk.threadcontrol import guithread
from accgtk.helpers import AccGtkHelpers
from accgtk.exceptioncatcher import catch_exception


class GuiInvoice(Invoice, Loadable):

	@staticmethod
	@guithread
	def empty_form(elements):
		elements.get_object("invoice_unique_label").set_text("- New invoice -")
		elements.get_object("invoice_short_label").set_text("- New invoice -")
		elements.get_object("invoice_name_entry").set_text("")
		elements.get_object("invoice_path_entry").set_text("")
		elements.get_object("invoice_amount_entry").set_text("")
		elements.get_object("invoice_sent_date_entry").set_text("")
		elements.get_object("invoice_days_of_grace_adjustment").set_value(30)
		elements.get_object("invoice_revoked_switch").set_active(False)
		elements.get_object("invoice_extended_by_dropdown").set_active_id("0")
		elements.get_object("invoice_date_of_settlement_entry").set_text("")
		elements.get_object("invoice_settlement_transaction_entry").set_text("")

	@guithread
	def to_gui(self, elements):
		elements.get_object("invoice_unique_label").set_text(str(self.unique))
		elements.get_object("invoice_short_label").set_text(self.short)
		elements.get_object("invoice_name_entry").set_text(AccGtkHelpers.none_to_empty(self.name))
		elements.get_object("invoice_path_entry").set_text(AccGtkHelpers.path_to_str(self.path))
		elements.get_object("invoice_amount_entry").set_text(self.figure)
		elements.get_object("invoice_sent_date_entry").set_text(AccGtkHelpers.datestr(self.sendDate))
		elements.get_object("invoice_days_of_grace_adjustment").set_value(self.daysOfGrace)
		elements.get_object("invoice_revoked_switch").set_active(self.revoked)
		elements.get_object("invoice_extended_by_dropdown").set_active_id(str(AccGtkHelpers.unique_or_zero(getattr(self.extendedBy, "unique", None))))
		elements.get_object("invoice_date_of_settlement_entry").set_text(AccGtkHelpers.datestr(self.dateOfSettlement))
		elements.get_object("invoice_settlement_transaction_entry").set_text(AccGtkHelpers.none_to_empty(self.settlementTransactionId))


class GuiInvoiceCreation(InvoiceCreation, Savable):

	@classmethod
	@catch_exception
	def from_gui(cls, elements, project_unique):

		return cls(
			project=project_unique,
			unique=AccGtkHelpers.unique_or_none(elements.get_object("invoice_unique_label").get_text()),
			name=AccGtkHelpers.empty_to_none(elements.get_object("invoice_name_entry").get_text()),
			path=AccGtkHelpers.empty_to_none(elements.get_object("invoice_path_entry").get_text()),
			figure=AccGtkHelpers.empty_to_none(elements.get_object("invoice_amount_entry").get_text()),
			sendDate=AccGtkHelpers.text_to_date(elements.get_object("invoice_sent_date_entry").get_text()),
			daysOfGrace=elements.get_object("invoice_days_of_grace_adjustment").get_value(),
			revoked=elements.get_object("invoice_revoked_switch").get_active(),
			extendedBy=AccGtkHelpers.unique_or_none(elements.get_object("invoice_extended_by_dropdown").get_active_id()),
			dateOfSettlement=AccGtkHelpers.text_to_date(elements.get_object("invoice_date_of_settlement_entry").get_text()),
			settlmentTransactionId=AccGtkHelpers.empty_to_none(elements.get_object("invoice_settlement_transaction_entry").get_text())
		)

