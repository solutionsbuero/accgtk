import os
import uuid
import yaml
from pathlib import Path
from accthymemodels.models.organization import Organization
from accgtk.guimodels.interfaces import Savable, Loadable
from accgtk.helpers import AccGtkHelpers


class GuiOrganization(Organization, Loadable, Savable):

	@staticmethod
	def empty_form(elements):
		elements.get_object("organization_name_entry").set_text("")
		elements.get_object("organization_street_entry").set_text("")
		elements.get_object("organization_postalCode_entry").set_text("")
		elements.get_object("organization_city_entry").set_text("")
		elements.get_object("organization_email_entry").set_text("")
		elements.get_object("organization_url_entry").set_text("")
		elements.get_object("organization_logo_entry").set_text("")
		elements.get_object("organization_currency_dropdown").set_active_id("0")

	def to_gui(self, elements):
		elements.get_object("organization_name_entry").set_text(self.name)
		elements.get_object("organization_street_entry").set_text(self.street)
		elements.get_object("organization_postalCode_entry").set_text(self.postalCode)
		elements.get_object("organization_city_entry").set_text(self.place)
		elements.get_object("organization_email_entry").set_text(AccGtkHelpers.none_to_empty(self.email))
		elements.get_object("organization_url_entry").set_text(AccGtkHelpers.none_to_empty(self.url))
		elements.get_object("organization_logo_entry").set_text(str(AccGtkHelpers.none_to_empty(self.logo)))
		elements.get_object("organization_currency_dropdown").set_active_id(str(getattr(self.currency, "value", "0")))

	@classmethod
	def from_gui(cls, elements):
		return cls(
			name=elements.get_object("organization_name_entry").get_text(),
			street=elements.get_object("organization_street_entry").get_text(),
			postalCode=elements.get_object("organization_postalCode_entry").get_text(),
			place=elements.get_object("organization_city_entry").get_text(),
			email=elements.get_object("organization_email_entry").get_text(),
			url=AccGtkHelpers.empty_to_none(elements.get_object("organization_url_entry").get_text()),
			logo=AccGtkHelpers.empty_to_none(elements.get_object("organization_logo_entry").get_text()),
			currency=AccGtkHelpers.currency_from_dropdown(elements.get_object("organization_currency_dropdown").get_active_id())
		)
