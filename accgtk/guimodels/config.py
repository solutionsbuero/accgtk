import os
import uuid
import yaml
from pathlib import Path
from accthymemodels.models.accthymeconfig import AccConfig, AccThymeConfig
from accgtk.guimodels.interfaces import Savable, Loadable
from accgtk.helpers import AccGtkHelpers


class GuiConfig(AccThymeConfig, Loadable, Savable):
	unique: uuid.UUID = uuid.uuid1()

	@staticmethod
	def get_configfile() -> Path:
		# Config path for Windows, something like C:\User\username\Appdata\Roaming
		if 'APPDATA' in os.environ:
			confighome = Path(os.environ['APPDATA']).resolve()
		# Config path for macOS
		elif 'XDG_CONFIG_HOME' in os.environ:
			confighome = Path(os.environ['XDG_CONFIG_HOME']).resolve()
		# Config path for Linux, /home/username/.config
		else:
			confighome = Path(os.environ['HOME']).resolve() / '.config'
		orgfile = confighome / 'accgtk' / 'organizations.yaml'
		if not orgfile.is_file():
			with orgfile.open("w") as f:
				f.write(yaml.safe_dump([]))
		return orgfile

	@staticmethod
	def empty_form(elements):
		elements.get_object("config_unique_label").set_text("- New organization -")
		elements.get_object("config_desc_entry").set_text("")
		elements.get_object("config_path_entry").set_text("")
		elements.get_object("config_autopull_switch").set_active(False)
		elements.get_object("config_autocommit_switch").set_active(False)

	def to_gui(self, elements):
		elements.get_object("config_unique_label").set_text(str(self.unique))
		elements.get_object("config_desc_entry").set_text(self.name)
		elements.get_object("config_path_entry").set_text(str(self.acc.basepath))
		elements.get_object("config_autopull_switch").set_active(self.acc.auto_pull)
		elements.get_object("config_autocommit_switch").set_active(self.acc.auto_commit)

	def save(self):
		fulldict = self.get_config_dict()
		fulldict[self.unique] = self
		fulllist = self._dict_to_list(fulldict)
		yamllist = [yaml.safe_load(i.yaml()) for i in fulllist]
		with self.get_configfile().open("w") as f:
			f.write(yaml.safe_dump(yamllist))
		return self

	def delete(self):
		fulldict = self.get_config_dict()
		fulldict.pop(self.unique, None)
		fulllist = self._dict_to_list(fulldict)
		yamllist = [yaml.safe_load(i.yaml()) for i in fulllist]
		with self.get_configfile().open("w") as f:
			f.write(yaml.safe_dump(yamllist))

	@classmethod
	def from_gui(cls, elements):
		unique = AccGtkHelpers.unique_or_none(elements.get_object("config_unique_label").get_text())
		if unique is None:
			unique = uuid.uuid1()
		acc_conf = AccConfig(
			basepath=elements.get_object("config_path_entry").get_text(),
			auto_pull=elements.get_object("config_autopull_switch").get_active(),
			auto_commit=elements.get_object("config_autocommit_switch").get_active()
		)
		return cls(
			unique=unique,
			name=elements.get_object("config_desc_entry").get_text(),
			acc=acc_conf
		)

	@classmethod
	def get_config_dict(cls) -> dict:
		configdict = {}
		with cls.get_configfile().open() as f:
			rawlist = yaml.safe_load(f.read())
		for i in rawlist:
			entry = cls.parse_obj(i)
			configdict[entry.unique] = entry
		return configdict

	@staticmethod
	def _dict_to_list(configdict: dict) -> list:
		configlist = []
		for k, v in configdict.items():
			configlist.append(v)
		configlist.sort(key=lambda x: x.name.lower())
		return configlist

	@classmethod
	def get_config_list(cls) -> list:
		configdict = cls.get_config_dict()
		return cls._dict_to_list(configdict)

	@classmethod
	def get_config_by_unique(cls, unique):
		configdict = cls.get_config_dict()
		unique = AccGtkHelpers.unique_or_none(unique)
		if unique is None:
			return None
		return configdict.get(unique, None)
