from accthymemodels.models.customer import Customer, CustomerCreation
from accgtk.guimodels.interfaces import Loadable, Savable
from accgtk.threadcontrol import guithread
from accgtk.helpers import AccGtkHelpers


class GuiCustomer(Customer, Loadable):

	@staticmethod
	@guithread
	def empty_form(elements):
		elements.get_object("customer_unique_label").set_text("- New customer -")
		elements.get_object("customer_short_label").set_text("- New customer -")
		elements.get_object("customer_name_entry").set_text("")
		elements.get_object("customer_street_entry").set_text("")
		elements.get_object("customer_postalCode_entry").set_text("")
		elements.get_object("customer_city_entry").set_text("")
		elements.get_object("customer_email_entry").set_text("")

	@guithread
	def to_gui(self, elements):
		elements.get_object("customer_unique_label").set_text(str(self.unique))
		elements.get_object("customer_short_label").set_text(self.short)
		elements.get_object("customer_name_entry").set_text(AccGtkHelpers.none_to_empty(self.name))
		elements.get_object("customer_street_entry").set_text(AccGtkHelpers.none_to_empty(self.street))
		elements.get_object("customer_postalCode_entry").set_text(AccGtkHelpers.none_to_empty(self.postalCode))
		elements.get_object("customer_city_entry").set_text(AccGtkHelpers.none_to_empty(self.place))
		elements.get_object("customer_email_entry").set_text(AccGtkHelpers.none_to_empty(self.email))


class GuiCustomerCreation(CustomerCreation, Savable):

	@classmethod
	def from_gui(cls, elements):
		return cls(
			unique=AccGtkHelpers.unique_or_none(elements.get_object("customer_unique_label").get_text()),
			name=AccGtkHelpers.empty_to_none(elements.get_object("customer_name_entry").get_text()),
			street=elements.get_object("customer_street_entry").get_text(),
			postalCode=elements.get_object("customer_postalCode_entry").get_text(),
			place=elements.get_object("customer_city_entry").get_text(),
			email=elements.get_object("customer_email_entry").get_text()
		)


