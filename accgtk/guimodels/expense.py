from accthymemodels.models.expense import Expense, ExpenseCreation
from accgtk.guimodels.interfaces import Loadable, Savable
from accgtk.threadcontrol import guithread
from accgtk.helpers import AccGtkHelpers


class GuiExpense(Expense, Loadable):

	@staticmethod
	def empty_form(elements):
		elements.get_object("expense_unique_label").set_text("- New expense -")
		elements.get_object("expense_short_label").set_text("- New expense -")
		elements.get_object("expense_name_entry").set_text("")
		elements.get_object("expense_path_entry").set_text("")
		elements.get_object("expense_amount_entry").set_text("")
		elements.get_object("expense_advanced_by_dropdown").set_active_id("0")
		elements.get_object("expense_category_dropdown").set_active_id("")
		elements.get_object("expense_billable_switch").set_active(False)
		elements.get_object("expense_internal_switch").set_active(False)
		elements.get_object("expense_payment_method_dropdown").set_active_id("banktransfer")
		elements.get_object("expense_accural_date_entry").set_text("")
		elements.get_object("expense_settlement_date_entry").set_text("")
		elements.get_object("expense_settlement_transaction_entry").set_text("")

	def to_gui(self, elements):
		elements.get_object("expense_unique_label").set_text(str(self.unique))
		elements.get_object("expense_short_label").set_text(self.short)
		elements.get_object("expense_name_entry").set_text(AccGtkHelpers.none_to_empty(self.name))
		elements.get_object("expense_path_entry").set_text(AccGtkHelpers.path_to_str(self.path))
		elements.get_object("expense_amount_entry").set_text(self.figure)
		elements.get_object("expense_advanced_by_dropdown").set_active_id(str(AccGtkHelpers.unique_or_zero(getattr(self.advancedBy, "unique", None))))
		elements.get_object("expense_category_dropdown").set_active_id(AccGtkHelpers.none_to_empty(self.expenseCategory))
		elements.get_object("expense_billable_switch").set_active(self.billable)
		elements.get_object("expense_internal_switch").set_active(self.internal)
		elements.get_object("expense_payment_method_dropdown").set_active_id(self.paymentMode.value)
		elements.get_object("expense_accural_date_entry").set_text(AccGtkHelpers.datestr(self.dateOfAccrual))
		elements.get_object("expense_settlement_date_entry").set_text(AccGtkHelpers.datestr(self.dateOfSettlement))
		elements.get_object("expense_settlement_transaction_entry").set_text(AccGtkHelpers.none_to_empty(self.settlementTransactionId))


class GuiExpenseCreation(ExpenseCreation, Savable):

	@classmethod
	def from_gui(cls, elements, project_unique):
		return cls(
			project=project_unique,
			unique=AccGtkHelpers.unique_or_none(elements.get_object("expense_unique_label").get_text()),
			name=AccGtkHelpers.empty_to_none(elements.get_object("expense_name_entry").get_text()),
			path=AccGtkHelpers.empty_to_none(elements.get_object("expense_path_entry").get_text()),
			figure=elements.get_object("expense_amount_entry").get_text(),
			advancedBy=AccGtkHelpers.unique_or_none(elements.get_object("expense_advanced_by_dropdown").get_active_id()),
			expenseCategory=AccGtkHelpers.empty_to_none(elements.get_object("expense_category_dropdown").get_active_id()),
			billable=elements.get_object("expense_billable_switch").get_active(),
			internal=elements.get_object("expense_internal_switch").get_active(),
			paymentMode=elements.get_object("expense_payment_method_dropdown").get_active_id(),
			dateOfAccrual=AccGtkHelpers.empty_to_none(elements.get_object("expense_accural_date_entry").get_text()),
			dateOfSettlement=AccGtkHelpers.empty_to_none(elements.get_object("expense_settlement_date_entry").get_text()),
			settlementTransactionId=AccGtkHelpers.empty_to_none(elements.get_object("expense_settlement_transaction_entry").get_text())
		)
