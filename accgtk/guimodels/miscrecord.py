from accthymemodels.models.miscrecord import MiscRecord, MiscRecordCreation
from accgtk.guimodels.interfaces import Loadable, Savable
from accgtk.threadcontrol import guithread
from accgtk.helpers import AccGtkHelpers


class GuiMiscRecord(MiscRecord, Loadable):

	@staticmethod
	def empty_form(elements):
		elements.get_object("miscrecord_unique_label").set_text("- New record -")
		elements.get_object("miscrecord_short_label").set_text("- New record -")
		elements.get_object("miscrecord_name_entry").set_text("")
		elements.get_object("miscrecord_path_entry").set_text("")
		elements.get_object("miscrec_accural_date_entry").set_text("")

	def to_gui(self, elements):
		elements.get_object("miscrecord_unique_label").set_text(str(self.unique))
		elements.get_object("miscrecord_short_label").set_text(self.short)
		elements.get_object("miscrecord_name_entry").set_text(AccGtkHelpers.none_to_empty(self.name))
		elements.get_object("miscrecord_path_entry").set_text(AccGtkHelpers.path_to_str(self.path))
		elements.get_object("miscrec_accural_date_entry").set_text(AccGtkHelpers.datestr(self.dateOfAccrual))


class GuiMiscRecordCreation(MiscRecordCreation, Savable):

	@classmethod
	def from_gui(cls, elements, project_unique):
		return cls(
			project=project_unique,
			unique=AccGtkHelpers.unique_or_none(elements.get_object("miscrecord_unique_label").get_text()),
			name=AccGtkHelpers.empty_to_none(elements.get_object("miscrecord_name_entry").get_text()),
			dateOfAccrual=AccGtkHelpers.empty_to_none(elements.get_object("miscrec_accural_date_entry").get_text()),
			path=AccGtkHelpers.empty_to_none(elements.get_object("miscrecord_path_entry").get_text())
		)
