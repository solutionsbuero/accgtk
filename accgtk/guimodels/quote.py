from accthymemodels.models.quote import Quote, QuoteCreation
from accgtk.guimodels.interfaces import Loadable, Savable
from accgtk.threadcontrol import guithread
from accgtk.helpers import AccGtkHelpers


class GuiQuote(Quote, Loadable):

	@staticmethod
	@guithread
	def empty_form(elements):
		elements.get_object("quote_unique_label").set_text("- New quote -")
		elements.get_object("quote_short_label").set_text("- New quote -")
		elements.get_object("quote_name_entry").set_text("")
		elements.get_object("quote_path_entry").set_text("")
		elements.get_object("quote_sent_date_entry").set_text("")
		elements.get_object("quote_days_valid_adjustment").set_value(30)
		elements.get_object("quote_accepted_switch").set_active(False)
		elements.get_object("quote_revoked_switch").set_active(False)

	@guithread
	def to_gui(self, elements):
		elements.get_object("quote_unique_label").set_text(str(self.unique))
		elements.get_object("quote_short_label").set_text(self.short)
		elements.get_object("quote_name_entry").set_text(AccGtkHelpers.none_to_empty(self.name))
		elements.get_object("quote_path_entry").set_text(AccGtkHelpers.path_to_str(self.path))
		elements.get_object("quote_sent_date_entry").set_text(AccGtkHelpers.datestr(self.sendDate))
		elements.get_object("quote_days_valid_adjustment").set_value(self.daysOfGrace)
		elements.get_object("quote_accepted_switch").set_active(self.accepted)
		elements.get_object("quote_revoked_switch").set_active(self.revoked)


class GuiQuoteCreation(QuoteCreation, Savable):

	@classmethod
	def from_gui(cls, elements, project_unique):
		return cls(
			project=project_unique,
			unique=AccGtkHelpers.unique_or_none(elements.get_object("quote_unique_label").get_text()),
			name=AccGtkHelpers.empty_to_none(elements.get_object("quote_name_entry").get_text()),
			path=AccGtkHelpers.empty_to_none(elements.get_object("quote_path_entry").get_text()),
			sendDate=AccGtkHelpers.text_to_date(elements.get_object("quote_sent_date_entry").get_text()),
			daysOfGrace=elements.get_object("quote_days_valid_adjustment").get_value(),
			accepted=elements.get_object("quote_accepted_switch").get_active(),
			revoked=elements.get_object("quote_revoked_switch").get_active()
		)
