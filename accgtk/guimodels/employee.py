from accthymemodels.models.employees import Employee, EmployeeCreation
from accgtk.guimodels.interfaces import Loadable, Savable
from accgtk.threadcontrol import guithread
from accgtk.helpers import AccGtkHelpers


class GuiEmployee(Employee, Loadable):

	@staticmethod
	def empty_form(elements):
		elements.get_object("employee_unique_label").set_text("- New employee -")
		elements.get_object("employee_short_label").set_text("- New employee -")
		elements.get_object("employee_name_entry").set_text("")
		elements.get_object("employee_street_entry").set_text("")
		elements.get_object("employee_postalCode_entry").set_text("")
		elements.get_object("employee_city_entry").set_text("")
		elements.get_object("employee_email_entry").set_text("")

	def to_gui(self, elements):
		elements.get_object("employee_unique_label").set_text(str(self.unique))
		elements.get_object("employee_short_label").set_text(self.short)
		elements.get_object("employee_name_entry").set_text(self.name)
		elements.get_object("employee_street_entry").set_text(AccGtkHelpers.none_to_empty(self.street))
		elements.get_object("employee_postalCode_entry").set_text(AccGtkHelpers.none_to_empty(self.postalCode))
		elements.get_object("employee_city_entry").set_text(AccGtkHelpers.none_to_empty(self.place))
		elements.get_object("employee_email_entry").set_text(self.email)


class GuiEmployeeCreation(EmployeeCreation, Savable):

	@classmethod
	def from_gui(cls, elements):
		return cls(
			unique=AccGtkHelpers.unique_or_none(elements.get_object("employee_unique_label").get_text()),
			name=AccGtkHelpers.empty_to_none(elements.get_object("employee_name_entry").get_text()),
			street=elements.get_object("employee_street_entry").get_text(),
			postalCode=elements.get_object("employee_postalCode_entry").get_text(),
			place=elements.get_object("employee_city_entry").get_text(),
			email=AccGtkHelpers.empty_to_none(elements.get_object("employee_email_entry").get_text())
		)
