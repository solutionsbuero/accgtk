from accthymemodels.models.reminder import Reminder, ReminderCreation
from accgtk.guimodels.interfaces import Loadable, Savable
from accgtk.threadcontrol import guithread
from accgtk.helpers import AccGtkHelpers


class GuiReminder(Reminder, Loadable):

	@staticmethod
	def empty_form(elements):
		elements.get_object("reminder_unique_label").set_text("- New quote -")
		elements.get_object("reminder_short_label").set_text("- New quote -")
		elements.get_object("reminder_name_entry").set_text("")
		elements.get_object("reminder_path_entry").set_text("")
		elements.get_object("reminder_sent_date_entry").set_text("")
		elements.get_object("reminder_days_of_grave_adjustment").set_value(15)
		elements.get_object("reminder_revoked_switch").set_active(False)
		elements.get_object("reminder_extended_by_dropdown").set_active_id("0")

	def to_gui(self, elements):
		elements.get_object("reminder_unique_label").set_text(str(self.unique))
		elements.get_object("reminder_short_label").set_text(self.short)
		elements.get_object("reminder_name_entry").set_text(AccGtkHelpers.none_to_empty(self.name))
		elements.get_object("reminder_path_entry").set_text(AccGtkHelpers.path_to_str(self.path))
		elements.get_object("reminder_sent_date_entry").set_text(AccGtkHelpers.datestr(self.sendDate))
		elements.get_object("reminder_days_of_grave_adjustment").set_value(self.daysOfGrace)
		elements.get_object("reminder_revoked_switch").set_active(self.revoked)
		elements.get_object("reminder_extended_by_dropdown").set_active_id(str(AccGtkHelpers.unique_or_zero(getattr(self.extendedBy, "unique", None))))


class GuiReminderCreation(ReminderCreation, Savable):

	@classmethod
	def from_gui(cls, elements, project_unique):
		return cls(
			project=project_unique,
			unique=AccGtkHelpers.unique_or_none(elements.get_object("reminder_unique_label").get_text()),
			name=AccGtkHelpers.empty_to_none(elements.get_object("reminder_name_entry").get_text()),
			path=AccGtkHelpers.empty_to_none(elements.get_object("reminder_path_entry").get_text()),
			sendDate=AccGtkHelpers.empty_to_none(elements.get_object("reminder_sent_date_entry").get_text()),
			daysOfGrace=elements.get_object("reminder_days_of_grave_adjustment").get_value(),
			revoked=elements.get_object("reminder_revoked_switch").get_active(),
			extendedBy=AccGtkHelpers.unique_or_none(elements.get_object("reminder_extended_by_dropdown").get_active_id())
		)
