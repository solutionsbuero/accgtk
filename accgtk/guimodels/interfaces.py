from abc import ABC, abstractmethod


class Loadable(ABC):

	@staticmethod
	@abstractmethod
	def empty_form(elements):
		pass

	@abstractmethod
	def to_gui(self, elements):
		pass


class Savable(ABC):

	@classmethod
	@abstractmethod
	def from_gui(cls, *args, **kwargs):
		pass
