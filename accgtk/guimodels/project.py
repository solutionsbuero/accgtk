from accthymemodels.models.enums import ProjectAlignment
from accthymemodels.models.project import Project, ProjectCreation
from accgtk.guimodels.interfaces import Loadable, Savable
from accgtk.threadcontrol import guithread
from accgtk.helpers import AccGtkHelpers


class GuiProject(Project, Loadable):

	@staticmethod
	@guithread
	def empty_form(elements):
		elements.get_object("project_unique_label").set_text("- New project -")
		elements.get_object("project_short_label").set_text("- New project -")
		elements.get_object("project_name_entry").set_text("")
		elements.get_object("project_alignment_dropdown").set_active_id(ProjectAlignment.REGULAR.value)

	@guithread
	def to_gui(self, elements):
		elements.get_object("project_unique_label").set_text(str(self.unique))
		elements.get_object("project_short_label").set_text(self.short)
		elements.get_object("project_name_entry").set_text(AccGtkHelpers.none_to_empty(self.name))
		elements.get_object("project_alignment_dropdown").set_active_id(self.alignment.value)


class GuiProjectCreation(ProjectCreation, Savable):

	@classmethod
	def from_gui(cls, elements, customer_unique):
		p = cls(
			customer=customer_unique,
			unique=AccGtkHelpers.unique_or_none(elements.get_object("project_unique_label").get_text()),
			name=AccGtkHelpers.empty_to_none(elements.get_object("project_name_entry").get_text()),
			alignment=elements.get_object("project_alignment_dropdown").get_active_id()
		)
		return p
