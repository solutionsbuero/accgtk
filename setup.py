from distutils.core import setup
from setuptools import find_packages

setup(
	name='accgtk',
	version='0.1.4',
	description='GTK frontend for acc erp system',
	author='Genossenschaft Solutionsbüro',
	author_email='info@buero.io',
	url='https://buero.io',
	packages=find_packages(),
	include_package_data=True,
	install_requires=[
		"pyyaml",
		"pygobject",
		"iso4217",
		"accthymemodels @ git+https://gitlab.com/solutionsbuero/accthymemodels.git"
	]
)
