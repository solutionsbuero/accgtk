# -*- mode: python ; coding: utf-8 -*-
import os
from PyInstaller.utils.hooks import collect_data_files
from pathlib import Path

block_cipher = None
spec_root = os.path.abspath(SPECPATH)

# Additional libs to pack
extra_imports = [
    'iso4217',
    'accthymemodels',
    # All accthymemodels dependencies:
    'pydantic',
    'peewee',
    'GitPython'
]

# Additional data files
added_files = []
added_files += collect_data_files('iso4217')
# Include Glade file & icon
glade_assets = [
    ('accgtk/icon/accgtk.svg', './accgtk/icon/'),
    ('accgtk/mainwindow.glade', './accgtk/')
]
added_files += glade_assets

a = Analysis(
    ['AccGTK.py'],
    pathex=['/home/dinu/git/solutionsbuero/AccThyme/accgtk'],
    binaries=[],
    datas=added_files,
    hiddenimports=extra_imports,
    hookspath=[],
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False
)

pyz = PYZ(
    a.pure,
    a.zipped_data,
    cipher=block_cipher
)

exe = EXE(
    pyz,
    a.scripts,
    [],
    exclude_binaries=True,
    name='AccGTK',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    console=True
)

coll = COLLECT(
    exe,
    a.binaries,
    a.zipfiles,
    a.datas,
    strip=False,
    upx=True,
    upx_exclude=[],
    name='AccGTK'
)

# Recursive function to remove unnecessary assets
def rm_stuff(pth: Path):
    for child in pth.iterdir():
        if not "adwaita" in str(child).lower():
            if child.is_file():
                child.unlink()
            else:
                rm_stuff(child)
                child.rmdir()

# Clean up unnecessary assets
sharepath = Path(spec_root).resolve() / "dist" / "AccGTK" / "share"
rm_stuff(sharepath / "themes")
rm_stuff(sharepath / "icons")
