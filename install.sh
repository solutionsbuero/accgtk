# Strait-forward bash installer for accgtk

# check if run as root
if [ "$EUID" -ne 0 ]
    then >&2 echo "ERROR: Please run me as root"
    exit 1
fi

# Determine distribution
echo "Determining distro/installed package manager..."

let DISTRO="none"

which apt
if [ $? -eq 0 ]; then
    DISTRO="debian"
    echo "apt is installed, probably a Debian based distro"
fi

which dnf
if [ $? -eq 0 ]; then
    DISTRO="fedora"
    echo "dnf is installed, probably a Fedora based distro"
fi

if [ $DISTRO == "none" ]; then
    >&2 echo "ERROR: Neither apt nor dnf is installed. This script is intended for Fedora or Debian-based distributions. Please install manually."
    exit 1
fi

# Dependency installation

echo "Installing dependencies..."

## RedHat/Fedora path
if [ $DISTRO == "fedora" ]; then
    dnf install -y python3 cairo-devel pkg-config python3-devel python3-gobject python3-gobject-devel cairo-gobject-devel
fi

## Debian path
if [ $DISTRO == "debian" ]; then
    apt-get install -y python3 python3-dev python3-venv python3-distutils libgirepository1.0-dev gcc libcairo2-dev pkg-config libgtk-4-dev
fi

if [ $? -ne 0 ]; then
    echo "ERROR: Dependency installation failed"
    exit 1
fi

# Create folder structure

echo "Installing application..."

mkdir -p /opt/accgtk
cd /opt/accgtk


# Create venv
python3 -m venv venv

if [ $? -ne 0 ]; then
    echo "ERROR: Creating virtual environment failed"
    exit 1
fi

# Install packages

. venv/bin/activate
pip3 install git+https://gitlab.com/solutionsbuero/accgtk.git


if [ $? -ne 0 ]; then
    echo "ERROR: pip-installing application failed"
    exit 1
fi

# Deploy assets

python3 -m accgtk.assets.extract -f accgtk.svg -d /opt/accgtk
python3 -m accgtk.assets.extract -f accgtk.sh -d /opt/accgtk
python3 -m accgtk.assets.extract -f update.sh -d /opt/accgtk
python3 -m accgtk.assets.extract -f uninstall.sh -d /opt/accgtk
python3 -m accgtk.assets.extract -f accgtk.desktop -d /usr/share/applications

echo "Done!"
