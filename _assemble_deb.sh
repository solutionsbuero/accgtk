# Move to file as working directory
cd "$(dirname "$0")"

# Get version number
TVERSION=$(cat setup.py | grep "version" | perl -pe '($_)=/([0-9]+([.][0-9]+)+)/')
TREVISION="${TVERSION}-1"

# Get architecture
ARCH=$(dpkg --print-architecture)

# Assemble package name
TNAME="accgtk_${TREVISION}_${ARCH}"

if [-f release/${TNAME}.deb]
    then
        rm release/${TNAME}.deb
fi

mkdir deb
cd deb

mkdir $TNAME

cp -r ../deb_assets/* $TNAME

# move wd to package folder
cd $TNAME

# Write current revision number to control file
search=$(cat DEBIAN/control | grep "Version")

replace="Version: ${TREVISION}"
# echo $replace
sed -i "s/$search/$replace/" DEBIAN/control

# Create opt folder structure
if [ -d "./opt/accgtk" ]
    then
        rm -rf opt/accgtk/*
else
    mkdir -p opt/accgtk
fi

cp -Rf ../../dist/AccGTK/* opt/accgtk/
pwd
echo "CREATING DEB PACKAGE..."
cd ..
dpkg-deb --build $TNAME

mv -f "${TNAME}.deb" ../release/

cd ..

rm -rf deb

echo "DONE!"
