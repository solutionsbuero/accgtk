import os
import sys

# Optimize environment variables based on os
if getattr(sys, 'frozen', False) and sys.platform == "linux":
	os.environ['XDG_DATA_DIRS'] = os.environ['XDG_DATA_DIRS'] + ":/usr/share"  # Make GTK Themes available
	del os.environ['GTK_EXE_PREFIX']  # Fix performance issues with GTK

from accgtk.__main__ import AccGTK
AccGTK.launch()
