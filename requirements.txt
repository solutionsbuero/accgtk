pyyaml
pygobject
pyinstaller
iso4217
git+https://gitlab.com/solutionsbuero/accthymemodels.git

# accthymemodels dependencies:
pydantic
peewee
GitPython
wheel
git+https://gitlab.com/solutionsbuero/accreport.git

# accreport dependencies
gitdb==4.0.7
pdfrw==0.4
Pillow==8.2.0
pylibdmtx==0.1.9  # Don't forget: install libdmtx0b first!
reportlab==3.5.66
smmap==4.0.0
typing-extensions==3.7.4.3
